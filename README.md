<table width="100%">
    <tr>
        <td width="25%">
            <img src="https://www.unige.ch/medecine/kinesiology/application/files/5615/7313/2686/logo_UNIGE_300.png" alt="Kinesiology Laboratory - University of Geneva" width="100%"/>
        </td>
        <td width="75%">
            <h1>Welcome on the K-LAB GitLab repository</h1><br>
            <p>Thank you for your interest in our projects. You can find further information about our research activites at the University of Geneva on our website: <a href="https://www.unige.ch/medecine/kinesiology" target="_blank">https://www.unige.ch/medecine/kinesiology</a>.</p>
            <p>The projects available on this repository are all freely available and opensource, under the license <a href="https://creativecommons.org/licenses/by-nc/4.0/" target="_blank">Creative Commons Attribution-NonCommercial 4.0 (International License)</a>. <strong>Not for clinical use</strong>.</p>
        </td>
    </tr>
</table>
<h2 align="left">PROJECT DESCRIPTION</h2>

* The proposed routine can be used to calculate gait events in gait trials based on Auto-Selection methodology.)

</h2>

## Important Notices

* `master` branch file paths (if exist) are **not** considered stable.


## Table of Contents
[**Installation**](#installation)

[**Features**](#features)

[**Dependencies**](#dependencies)

[**Developer**](#developer)

[**Examples**](#examples)

[**References**](#references)

[**License**](#license)

## Installation

* You just need to download or clone the project to use it. Just ensure that you are using Matlab R2018b or newer (The Mathworks, USA).

## Features

* GEV_Main
    * 1 - Calculate reference database: extract gait event frames by GRF + discrete points from marker trajectories at same frames
    * 2 - Model implementation
    * 3 - Detection of most accurate model
    * 4 - Results visualization
)

## Dependencies

* Biomechanical-ToolKit by Arnaud Barré, freely available here: https://github.com/Biomechanical-ToolKit/BTKCore

## Developer

* The proposed routine has been developed by <a href="https://www.unige.ch/medecine/kinesiology/people/mickaelf/" target="_blank">Mickael Fonseca</a>, K-Lab, University of Geneva.

## References

* <b><a href="https://10.1016/j.gaitpost.2022.06.001" target="_blank">Automatic gait event detection in pathologic gait using an auto-selection methodology among concurrent methods</a></b><br>
M. Fonseca, R. Dumas, S. Armand<br>
<i>Gait & Posture 96 (271-274)</i>

## License
<a href="https://creativecommons.org/licenses/by-nc/4.0/legalcode" target="_blank">LICENSE</a> © K-Lab, University of Geneva
> Not for clinical use
