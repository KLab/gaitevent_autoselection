function [ev_diff, ev_diff_fr] = calculate_difference_evenets(event_m1, event_m2, frate, ev_diff, ev_diff_fr)
if length(event_m1) == length(event_m2)
    ev_diff = [ev_diff, mean(event_m1-event_m2)/frate];
    ev_diff_fr = [ev_diff_fr, mean(event_m1-event_m2)];
else
    if length(event_m1) > length(event_m2)
        if abs(event_m2(1)-event_m1(1)) <10
            event_m1 = event_m1(1:end-1);
        elseif abs(event_m2(1)-event_m1(2)) <10
            event_m1 = event_m1(2:end)
        end
    elseif length(event_m1) < length(event_m2)
        if abs(event_m1(1)-event_m2(1)) <10
            event_m2 = event_m2(1:end-1);
        elseif abs(event_m1(1)-event_m2(2)) <10
            event_m2 = event_m2(2:end)
        end
    end
    disp('Different number of events')
%     ev_diff = [ev_diff, mean(event_m1-event_m2)/frate];
%     ev_diff_fr = [ev_diff_fr, mean(event_m1-event_m2)];
end
end