%% routine to calculate confidence interval of a set of measurements
% @author M. Fonseca Jun 2021

function CI = ConfidenceInterval(x)
    SEM = std(x, 'omitnan')/sqrt(length(x));
    ts = tinv([0.025 0.975], length(x)-1);
    CI = mean(x, 'omitnan') + ts*SEM;
end