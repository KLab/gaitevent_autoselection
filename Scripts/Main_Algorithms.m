%% Main_Algorithms_Geneva
% Main sheet for generating gait events from kinematic data

% Codes connected to Visscher & Sansgiri et al. "Towards validation and
% standardization of automatic gait event identification algorithms for use
% in paediatric pathological populations".
% Current version is with TOE instead of HLX, and optimized for overall gait patterns

% Needed to run:
% 1. btk installed [http://biomechanical-toolkit.github.io/]
% 2. c3d files with marker trajectories of LTOE/RTOE, LHEE/RHEE, SACR
% 3. Matlab functions provided by Visscher & Sansgiri et al. 

% Outcomes: Events struct indicating frame on which gait event is estimated
% to happen

% Date: 11.5.2021
% Sailee Sansgiri & Rosa Visscher
% contact: bt@ethz.ch

close all;
clc;

%% Inital set-up

Path_data = uigetdir([],'Select folder with c3d files'); % location c3d files you want to extract gait events from
Path_func = uigetdir([],'Select folder with gait event functions'); % location were functions are saved to estimate gait events
% Path_btk = uigetdir([],'Select folder with btk functions'); % location were btk folder from biomechanical toolkit is saved
Path_save = uigetdir([],'Select folder to save outcomes'); % location to save outcomes

% addpath(genpath(Path_btk)); % add path towards btk
addpath(genpath(Path_func)); %add path to functions to estimate gait events
addpath(genpath(Path_data)); % add path towards c3d files

cd(Path_data) %go to folder in which c3d files are saved

%% Initialize variables
DIR_c3d=dir(Path_data); % names of c3d files which you would like to evaluate
DIR_c3d=DIR_c3d(~ismember({DIR_c3d.name},{'.','..'}));% remove empty lines
Events= struct;
Fig=1;%1=generate and save plots; 0=no plots are geenrate or saved

%% Define Markers 

% Define which markers you want to use to identify initial contact and
% toe-off
% Overall gait patterns, HEE marker showed best for indicating IC. 
% However for toe or flat-foot walkers select TOE instead.
% Overall and per subgroup, HXL marker showed best results for estimating TO.
% If HLX isn't available use TOE.
% Marker velocity can be calculated with either SACR or T10 in this current
% version.

IC_MarkerName_L = questdlg('Which marker do you want to use for initial contact detection left?', ...
    'Select marker', ...
    'LHEE','LTOE','Neither','Neither');
TO_MarkerName_L = questdlg('Which marker do you want to use for toe-off detection left?', ...
    'Select marker', ...
    'LHLX','LTOE','Neither','Neither');
IC_MarkerName_R = questdlg('Which marker do you want to use for initial contact detection right?', ...
    'Select marker', ...
    'RHEE','RTOE','Neither','Neither');
TO_MarkerName_R = questdlg('Which marker do you want to use for toe-off detection right?', ...
    'Select marker', ...
    'RHLX','RTOE','Neither','Neither');
Velo_MarkerName = questdlg('Which marker do you want to use for velocity estimation?', ...
    'Select marker', ...
    'SACR','T10','Neither','Neither');

% for-loop to perform calculations for all c3d files
for i=1:length(DIR_c3d)
    cd(Path_data) 
    c3dfile = cellstr(DIR_c3d(i).name);% select a c3d file
    c3d_name = split(c3dfile,'.');
    
    % Load c3d with btl
    btkData=btkReadAcquisition(c3dfile{1,1});
    btkClearEvents(btkData);
    metadata=btkGetMetaData(btkData);
    ff=btkGetFirstFrame(btkData);
    Markers = btkGetMarkers(btkData);
    angles=btkGetAngles(btkData);
    f = btkGetPointFrequency(btkData);
    freq(i,1)=f;
    n = btkGetPointFrameNumber(btkData);
    
    %% Correct for Walking Direction
    Velo_Marker = Markers.(Velo_MarkerName);
    
    % delete zeros at the beginning or end of an trial
    dir_i = abs(Velo_Marker(end, 1) - Velo_Marker(1, 1));
    dir_j = abs(Velo_Marker(end, 2) - Velo_Marker(1, 2));
    
    walkdir = 1;  % x is walkdir
    
    if (dir_i < dir_j)
        walkdir = 2;  % y is walkdir
    end
    
    % pos. or neg. direktion on axis
    sgn = sign(Velo_Marker(end, walkdir) - Velo_Marker(1, walkdir));
    walkdir = walkdir * sgn;
    [Markers_Corrected]=f_rotCoordinateSystem(Markers, walkdir, 1);
    gaitAxis=1;
    verticalAxis=3;
    
    %% Filtering Markers and preprocessing
    [B,A] = butter(4,6/(f/2),'low');

    filt_IC_marker_L = [];
    filt_IC_marker_R = [];
    filt_TO_marker_L = [];
    filt_TO_marker_R = [];
    filt_Velo_marker=[];
    filt_IC_marker_L(:,:,1) = filtfilt(B, A, Markers_Corrected.(IC_MarkerName_L));
    filt_TO_marker_L(:,:,1) = filtfilt(B, A, Markers_Corrected.(TO_MarkerName_L));
    filt_IC_marker_R(:,:,1) = filtfilt(B, A, Markers_Corrected.(IC_MarkerName_R));
    filt_TO_marker_R(:,:,1) = filtfilt(B, A, Markers_Corrected.(TO_MarkerName_R));
    filt_Velo_marker(:,:,1) = filtfilt(B, A, Markers_Corrected.(Velo_MarkerName));
    
    y_velo=filt_Velo_marker(:,gaitAxis,:);
    z_velo=filt_Velo_marker(:,verticalAxis,:);
    
    %Determine approximate walking speed
    [vel,time]=f_approxVelocity(y_velo,z_velo,f);
    vel2=vel/100;
    
    %% Kinematic Algorithm_Ghoussayni - initial contact
    [IC_L,~] = f_Ghoussayni_500(filt_IC_marker_L,filt_TO_marker_L,gaitAxis,verticalAxis,n,f);
    [IC_R,~] = f_Ghoussayni_500(filt_IC_marker_R,filt_TO_marker_R,gaitAxis,verticalAxis,n,f);
    
    %% Kinematic Algorithm_ModifiedGhoussayni - toe-off
    [~,TO_L]=f_Ghoussayni_variablethreshold(filt_IC_marker_L,filt_TO_marker_L,gaitAxis,verticalAxis,n,f,vel2);
    [~,TO_R]=f_Ghoussayni_variablethreshold(filt_IC_marker_R,filt_TO_marker_R,gaitAxis,verticalAxis,n,f,vel2);
    
    %% Collect outcomes in struct
    filename = string(c3d_name{1,1});
    Events.filename.Left.IC = IC_L;
    Events.filename.Left.TO = TO_L;
    if Events.filename.Left.IC(:,1)==1
        Events.filename.Left.IC(:,1) = [];
    end
    Events.filename.Right.IC = IC_R;
    Events.filename.Right.TO = TO_R;
    Events.filename.Markers = {IC_MarkerName_L, TO_MarkerName_L, IC_MarkerName_R, TO_MarkerName_R, Velo_MarkerName};
    if Events.filename.Right.IC(:,1)==1
        Events.filename.Right.IC(:,1) = [];
    end
    
    
    %% Plot Outcomes
    if Fig == 1
        figure
        subplot(2,1,1) %Left
        hold on
        plot(filt_TO_marker_L(:,end),'b')
        plot(filt_IC_marker_L(:,end),'r')
        for n=1:length(Events.filename.Left.TO)
            plot(Events.filename.Left.TO(1,n),filt_TO_marker_L(Events.filename.Left.TO(1,n),end),'*b')
        end
        for m=1:length(Events.filename.Left.IC)
            plot(Events.filename.Left.IC(1,m),filt_IC_marker_L(Events.filename.Left.IC(1,m),end),'*r')
        end
        title_name = join(['Estimated gait events for' c3dfile '- Left']);
        title(title_name)
        subplot(2,1,2) %Right
        hold on
        plot(filt_TO_marker_R(:,end),'b')
        plot(filt_IC_marker_R(:,end),'r')
        for n=1:length(Events.filename.Right.TO)
            plot(Events.filename.Right.TO(1,n),filt_TO_marker_R(Events.filename.Right.TO(1,n),end),'*b')
        end
        for m=1:length(Events.filename.Right.IC)
            plot(Events.filename.Right.IC(1,m),filt_IC_marker_R(Events.filename.Right.IC(1,m),end),'*r')
        end
        title_name = join(['Estimated gait events for' c3dfile '- Right']);
        title(title_name)
        
        cd(Path_save) 
        print(Fig,filename,'-dpng') 
    end
    
   
    
end %FOR-loop c3d files

%% Save outcomes in struct and table
subjects=fieldnames(Events);
for i=1:length(subjects)
    markers(i,:)= Events.(subjects{i,1}).Markers;
    IC_left(i,:)= {Events.(subjects{i,1}).Left.IC};
    TO_left(i,:)= {Events.(subjects{i,1}).Left.TO};
    IC_right(i,:)= {Events.(subjects{i,1}).Right.IC};
    TO_right(i,:)= {Events.(subjects{i,1}).Right.TO};
end

T_Events = table(subjects,markers(:,1),markers(:,2),markers(:,3),markers(:,4),markers(:,5),IC_left,TO_left,IC_right,TO_right);

cd(Path_save) 
save('Events_outcomes.mat','Events');
save('T_Events_outcomes.mat','T_Events');



