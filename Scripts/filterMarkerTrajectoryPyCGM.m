% Filter the given node with a zero-lag Butterworth filter.
%
% NOTE: This function tries to cast the node as a ma.TimeSequence object.
% In case, this is not such kind of object, an error is thrown
%
% NOTE: This function manages data occlusion based on the content of the
% residuals (last column). In case of data occlusion, the signal is filtered
% by part. In case the length of a part is lower than the required boundary
% reflection, this one is set to 0, and the associated residual is set to
% invalid.
%
% NOTE: As explained in Robertson & Dowling (2003) or Winter (2009),
% the order and cutoff frequency must be adapted due to the use of a zero-lag
% filter.

% Copyright (C) 2016, Moveck Solution Inc., all rights reserved.

function filterMarkerTrajectoryPyCGM(C3D, cutoff, order,Markers)
 fs=C3D.fRate.Point;
%            [b,a] = butter(4,30/(fs/2),'high');%200/fs/2
           [b,a] = butter(order,cutoff/(fs/2));%200/fs/2
for i=1:length(Markers)
   C3D.data.(Markers{i}) = filtfilt(b,a,C3D.data.(Markers{i}));
   btkSetPoint(C3D.acq, strcat(Markers{i}), C3D.data.(Markers{i}));
end