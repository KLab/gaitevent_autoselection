function [Lfoot, Rfoot, check_evfr, FPnumber_left, FPnumber_right] =  Check_Foot_ForcePlate_3(C3D, threshold_FP_detection, Lfoot,Rfoot, foot_wide_threshold, foot_front_threshold, check_evfr, n)
% check validity of gait event (1 foot completely inside platform) 
% input: 
%   threshold_FP_detection: minimal value of Force(z) to detect events
%   foot: Structure foot for export
%   foot_wide_threshold: half wide of foot considered to built the foot
%   foot_front_threshold: extra length from toe to extremity of foot considered to built the foot
% output:
%   foot: Structure foot for export updated
%   check_evfr: Struct. Frames of events detected per trial.
% version May 2021 for GEV project


%% 1.1 Get data
filename = C3D.filename;
m    = C3D.data;
fp  = btkGetForcePlatforms(C3D.acq);
frate = btkGetPointFrequency(C3D.acq);
forces_FP = btkGetForcePlatforms(C3D.acq);
metadata = btkGetMetaData(C3D.acq);
ff = btkGetFirstFrame(C3D.acq);
lf = btkGetLastFrame(C3D.acq);
values={'Events from Kinematics with autocorrelation with forceplate'};
format='Char';
INFO = btkMetaDataInfo(format, values) ;
% Count the number of force plates
nPF_used = metadata.children.FORCE_PLATFORM.children.USED.info.values;
%% 1.2 Get sense (sense=1 -> pf 1 to 2, sense=2 -> pf 2 to 1)
if nPF_used>1
    % Sense/direction of trial
    P1 = fp(1).corners;
    P2 = fp(2).corners;
    center_1 = (P1(1:2,1)/2 + P1(1:2,3)/2);
    center_2 = (P2(1:2,1)/2 + P2(1:2,3)/2);
    if isfield(m, 'C7')
       dist_P1 = pdist([m.C7(10,1), m.C7(10,2); center_1(1), center_1(2)], 'euclidean');
       dist_P2 = pdist([m.C7(10,1), m.C7(10,2); center_2(1), center_2(2)], 'euclidean');
    else
        dist_P1 = pdist([m.LASI(10,1), m.LASI(10,2); center_1(1), center_1(2)], 'euclidean');
        dist_P2 = pdist([m.LASI(10,1), m.LASI(10,2); center_2(1), center_2(2)], 'euclidean');
    end
%     if isnan(dist_P1) || isnan(dist_P2)
%         mes = '[ERROR] the marker coordinate used to calculate sense invalid. Check for NaN values';
%         error(mes)
%     end
    if dist_P1 < dist_P2
        sense = 1;  %from plat 1 to 2
    elseif dist_P1 > dist_P2
        sense = -1;   %from plat 2 to 1
    end
end

%% 2 Detection
for nPF = 1:nPF_used
    disp(['     - Platform: ', num2str(nPF)])
    % Force data (only vertical component) from ForcePlatforms 
    if isfield(forces_FP(nPF).channels, 'Fz')
        FP = forces_FP(nPF).channels.('Fz');
    elseif isfield(forces_FP(nPF).channels, string(strcat('Fz',num2str(nPF))))
        FP = forces_FP(nPF).channels.(strcat('Fz',num2str(nPF)));
    end
    
    % frame foot strike for each platform
    FP_detect = find(abs(FP)>threshold_FP_detection);
    if ~isempty(FP_detect)
        FP_FS_f = round(FP_detect(1)/10)+ff;
        FP_FO_f = round(FP_detect(end)/10)+ff;
        if FP_FS_f > ff && FP_FO_f < lf
            % get 4 corners of platform
            Corners_FP = fp(nPF).corners;
            %check if foot valid in platform(nFP)
            [lfs_f, lfo_f, rfs_f, rfo_f] = Check_Foot_ForcePlate_GE(Corners_FP, m,sense,foot_wide_threshold,foot_front_threshold, FP_FS_f, FP_FO_f,ff);
            % if valid in platform at both moments (FO,FS), store the coordinate of HEE and TOE on both moments for the left and right side separately
            if ~isempty(lfs_f) && ~isempty(lfo_f)
                %Normalize all vector between 0-1
                norm_LHEEz = Normalize_vectors( m.LHEE(:,3));
                norm_LTOEz = Normalize_vectors(m.LTOE(:,3));
                norm_Foot_far_LHEEfs = Normalize_vectors(sense*(m.LHEE(:,1)-m.SACR(:,1)));
                norm_Foot_far_LTOEfo = Normalize_vectors(sense*(m.LTOE(:,1)-m.SACR(:,1)));
                norm_Hipx_fs = Normalize_vectors(sense*(m.LASI(:,1)-m.RASI(:,1)));
                norm_footangle_fo = Normalize_vectors(m.LHEE(:,3)-m.LTOE(:,3));
                foot = [(m.LTOE(:,1)-m.LHEE(:,1)) (m.LTOE(:,3)-m.LHEE(:,3))];
                floor = [(m.LTOE(:,1)-m.LHEE(:,1)) zeros(length(m.LTOE),1)];
                VdotF = (foot(:,1).*floor(:,1) + foot(:,2).*floor(:,2));
                norm_footang = Normalize_vectors(acosd(VdotF/(norm(floor)*norm(foot))));
                
                % Foot strike discrete parameters to extract
                Lfoot(n).LHEEz_FS = norm_LHEEz(lfs_f-ff);
                Lfoot(n).Foot_far_FS = norm_Foot_far_LHEEfs(lfs_f-ff);
                Lfoot(n).LHipx_fs = norm_Hipx_fs(lfs_f-ff);
                Lfoot(n).LFA_fs = norm_footang(lfs_f-ff);
                
                % Foot off discrete parameters to extract
                Lfoot(n).LTOEz_FO = norm_LTOEz(lfo_f-ff);
                Lfoot(n).Foot_far_FO = norm_Foot_far_LTOEfo(lfo_f-ff);
                Lfoot(n).footangle_FO = norm_footangle_fo(lfo_f-ff);
                Lfoot(n).LFA_FO = norm_footang(lfo_f-ff);

                btkAppendEvent(C3D.acq, 'Lfoot Strike', (lfs_f-1)/frate, 'Left','','',1);
                btkAppendEvent(C3D.acq, 'Lfoot Off', (lfo_f-1)/frate, 'Left', '', '',2);
                check_evfr(n).LFS=lfs_f;
                check_evfr(n).LFO=lfo_f;
                check_evfr(n).Left_flag = 'True';
                check_evfr(n).Filename = filename;
                Lfoot(n).Filename = filename;
                Lfoot(n).FP_number = nPF;
                Lfoot(n).LFS = lfs_f;
                Lfoot(n).LFO = lfo_f;
            else
                check_evfr(n).Left_flag = 'False';
                check_evfr(n).Filename = filename;

            end
            %% Right side
            
            if ~isempty(rfs_f) && ~isempty(rfo_f)
                %Normalize all vector between 0-1
                norm_RHEEz = Normalize_vectors( m.RHEE(:,3));
                norm_RTOEz = Normalize_vectors(m.RTOE(:,3));
                norm_Foot_far_RHEEfs = Normalize_vectors(sense*(m.RHEE(:,1)-m.SACR(:,1)));
                norm_Foot_far_RTOEfo = Normalize_vectors(sense*(m.RTOE(:,1)-m.SACR(:,1)));
                norm_Hipx_fs = Normalize_vectors(sense*(m.RASI(:,1)-m.LASI(:,1)));
                norm_footangle_fo = Normalize_vectors(m.RHEE(:,3)-m.RTOE(:,3));
                foot = [(m.RTOE(:,1)-m.RHEE(:,1)) (m.RTOE(:,3)-m.RHEE(:,3))];
                floor = [(m.RTOE(:,1)-m.RHEE(:,1)) zeros(length(m.RTOE),1)];
                VdotF = (foot(:,1).*floor(:,1) + foot(:,2).*floor(:,2));
                norm_footang = Normalize_vectors(acosd(VdotF/(norm(floor)*norm(foot))));
                
                % Foot strike
                Rfoot(n).RHEEz_FS = norm_RHEEz(rfs_f-ff);
                Rfoot(n).Foot_far_FS = norm_Foot_far_RHEEfs(rfs_f-ff);
                Rfoot(n).RHipx_fs = norm_Hipx_fs(rfs_f-ff);
                Rfoot(n).RFA_fs = norm_footang(rfs_f-ff);
                
                % Foot off
                Rfoot(n).RTOEz_FO = norm_RTOEz(rfo_f-ff);
                Rfoot(n).Foot_far_FO = norm_Foot_far_RTOEfo(rfo_f-ff);
                Rfoot(n).footangle_FO = norm_footangle_fo(rfo_f-ff);
                Rfoot(n).RFA_FO = norm_footang(rfo_f-ff);
                
                btkAppendEvent(C3D.acq, 'Rfoot Strike', (rfs_f-1)/frate, 'Right','','',1);
                btkAppendEvent(C3D.acq, 'Rfoot Off', (rfo_f-1)/frate, 'Right', '', '',2);
                check_evfr(n).RFS=rfs_f;
                check_evfr(n).RFO=rfo_f;
                check_evfr(n).Right_flag = 'True';
                check_evfr(n).Filename = filename;
                Rfoot(n).Filename = filename;
                Rfoot(n).FP_number = nPF;
                Rfoot(n).RFS = rfs_f;
                Rfoot(n).RFO = rfo_f;
            else
                check_evfr(n).Right_flag = 'False';
                check_evfr(n).Filename = filename;

            end
        else
            disp(['Warning: ', filename, ' First frame equals to signal detected in platform!'])
        end
        
    else
        disp(['Warning: ', filename, ' Platform not found! '])
    end
    
    % %% Update MetaData
    btkAppendMetaData(C3D.acq, 'EVENT', 'Method',INFO);
    btkWriteAcquisition(C3D.acq,[C3D.pathname, filename]);
    n = n+1;
end
end