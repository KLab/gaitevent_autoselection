function [LFS_f,LFO_f,RFS_f,RFO_f] = Check_Foot_ForcePlate_GE(fp_corners, m ,sense,foot_wide_threshold,foot_front_threshold, FP_FS_f, FP_FO_f,ff)
% description: Detection of foot steps clean inside force platforms (FP)
%
% input:
% fp_corners - spatial coordinates of FP corners (matrix 3x4)
% m - structure m containing marker coordinates
% sense - sense of walk (1 or -1)
% foot_wide_threshold - increment to foot wide
% foot_front_threshold - increment to foot length
% FP_FS_f - foot strike frame
% FP_FO_f - foot off frame
% ff - first frame of acquisition
%
% output:
% LFS_f/RFS_f - validity of step for the foot strike event (L-left/R-right)
% LFO_f/RFO_f - validity of step for the foot off event (L-left/R-right)

%% FOOT STRIKE
% Get the coordinates of foot markers on the frame of interest
RHEE_FS = m.RHEE(FP_FS_f-ff,:); 
LHEE_FS = m.LHEE(FP_FS_f-ff,:);
RTOE_FS = m.RTOE(FP_FS_f-ff,:); 
LTOE_FS = m.LTOE(FP_FS_f-ff,:); 

% initialize output variables
LFS_f = [];
RFS_f = [];
LFO_f = [];
RFO_f = [];

% size of the feet at foot strike 
Length_LFoot = abs(LHEE_FS(1,1)-LTOE_FS(1,1))*foot_front_threshold;
Length_RFoot = abs(RHEE_FS(1,1)-RTOE_FS(1,1))*foot_front_threshold;
Left_width = (foot_wide_threshold*Length_LFoot)/2;
Right_width = (foot_wide_threshold*Length_RFoot)/2;

% define foot rectangle according to sense of walking
% if sense == 1                                          % direction platform 1 -> 2
%     lf_corners  = [LHEE_FS(1), LHEE_FS(2)-Left_width; LHEE_FS(1)+Length_LFoot, LHEE_FS(2)-Left_width;LHEE_FS(1)+Length_LFoot, LHEE_FS(2)+Left_width; LHEE_FS(1), LHEE_FS(2)+Left_width];
%     rf_corners  = [RHEE_FS(1), RHEE_FS(2)-Right_width; RHEE_FS(1)+Length_RFoot, RHEE_FS(2)-Right_width;RHEE_FS(1)+Length_RFoot, RHEE_FS(2)+Right_width; RHEE_FS(1), RHEE_FS(2)+Right_width];
% else                                                   % direction platform 2 -> 1
%     lf_corners  = [LHEE_FS(1), LHEE_FS(2)+Left_width; LHEE_FS(1)-Length_LFoot, LHEE_FS(2)+Left_width;LHEE_FS(1)-Length_LFoot, LHEE_FS(2)-Left_width; LHEE_FS(1), LHEE_FS(2)-Left_width];
%     rf_corners  = [RHEE_FS(1), RHEE_FS(2)+Right_width; RHEE_FS(1)-Length_RFoot, RHEE_FS(2)+Right_width;RHEE_FS(1)-Length_RFoot, RHEE_FS(2)-Right_width; RHEE_FS(1), RHEE_FS(2)-Right_width];
% end
if sense == 1                                          % direction platform 1 -> 2
    lf_corners  = [LHEE_FS(1), LHEE_FS(2)-Left_width; LHEE_FS(1)+Length_LFoot, LTOE_FS(2)-Left_width;LHEE_FS(1)+Length_LFoot, LTOE_FS(2)+Left_width; LHEE_FS(1), LHEE_FS(2)+Left_width];
    rf_corners  = [RHEE_FS(1), RHEE_FS(2)-Right_width; RHEE_FS(1)+Length_RFoot, RTOE_FS(2)-Right_width;RHEE_FS(1)+Length_RFoot, RTOE_FS(2)+Right_width; RHEE_FS(1), RHEE_FS(2)+Right_width];
else                                                   % direction platform 2 -> 1
    lf_corners  = [LHEE_FS(1), LHEE_FS(2)+Left_width; LHEE_FS(1)-Length_LFoot, LTOE_FS(2)+Left_width;LHEE_FS(1)-Length_LFoot, LTOE_FS(2)-Left_width; LHEE_FS(1), LHEE_FS(2)-Left_width];
    rf_corners  = [RHEE_FS(1), RHEE_FS(2)+Right_width; RHEE_FS(1)-Length_RFoot, RTOE_FS(2)+Right_width;RHEE_FS(1)-Length_RFoot, RTOE_FS(2)-Right_width; RHEE_FS(1), RHEE_FS(2)-Right_width];
end
% function that reports if the 4 corners of foot are inside FP (1 for yes 0 for no, [1x4])
r_FS_inside = insidePlatform(rf_corners, fp_corners);
l_FS_inside = insidePlatform(lf_corners, fp_corners);

if sum(r_FS_inside) == 4 %if all points of R foot in
   if sum(l_FS_inside) > 0 % if at least one point of L foot in both feet are invalid
     RFS_f = RFS_f;
     LFS_f = LFS_f;
   else % if no point of L foot in then R foot in platform is validated
     RFS_f = [RFS_f, FP_FS_f];
     LFS_f = LFS_f;
   end
elseif sum(r_FS_inside) == 0 % if none point of R foot in platform 
    RFS_f = RFS_f;
    if sum(l_FS_inside) == 4 % if all points of L foot in platform is validated
        LFS_f = [LFS_f, FP_FS_f];
    else % else none feet are validated
        LFS_f = LFS_f;
    end
else 
    RFS_f = [];
    LFS_f = LFS_f;
end
    
%% FOOT OFF (same principle of Foot strike)
RHEE_FO = m.RHEE(FP_FO_f-ff,:);                        
LHEE_FO = m.LHEE(FP_FO_f-ff,:);
RTOE_FO = m.RTOE(FP_FO_f-ff,:); 
LTOE_FO = m.LTOE(FP_FO_f-ff,:); 
 
% size of the feet at foot off (x component)
 Length_LFoot = abs(LHEE_FO(1,1)-LTOE_FO(1,1))*foot_front_threshold;
 Length_RFoot = abs(RHEE_FO(1,1)-RTOE_FO(1,1))*foot_front_threshold;
if sense == 1     % direction platform 1 -> 2
    lf_corners  = [LHEE_FO(1), LHEE_FO(2)-Left_width; LHEE_FO(1)+Length_LFoot, LHEE_FO(2)-Left_width;LHEE_FO(1)+Length_LFoot, LHEE_FO(2)+Left_width; LHEE_FO(1), LHEE_FO(2)+Left_width];
    rf_corners  = [RHEE_FO(1), RHEE_FO(2)-Right_width; RHEE_FO(1)+Length_RFoot, RHEE_FO(2)-Right_width;RHEE_FO(1)+Length_RFoot, RHEE_FO(2)+Right_width; RHEE_FO(1), RHEE_FO(2)+Right_width];
elseif sense == 2 % direction platform 2 -> 1
    lf_corners  = [LHEE_FO(1), LHEE_FO(2)+Left_width; LHEE_FO(1)-Length_LFoot, LHEE_FO(2)+Left_width;LHEE_FO(1)-Length_LFoot, LHEE_FO(2)-Left_width; LHEE_FO(1), LHEE_FO(2)-Left_width];
    rf_corners  = [RHEE_FO(1), RHEE_FO(2)+Right_width; RHEE_FO(1)-Length_RFoot, RHEE_FO(2)+Right_width;RHEE_FO(1)-Length_RFoot, RHEE_FO(2)-Right_width; RHEE_FO(1), RHEE_FO(2)-Right_width];
end

r_FO_inside = insidePlatform(rf_corners, fp_corners);
l_FO_inside = insidePlatform(lf_corners, fp_corners);

if sum(r_FO_inside) == 4 %if all points of R foot in
   if sum(l_FO_inside) > 0 % if at least one point of L foot in both feet are invalid
     RFO_f = RFO_f;
     LFO_f = LFO_f;
   else % if no point of L foot in then R foot in platform is validated
     RFO_f = [RFO_f,FP_FO_f];
     LFO_f = LFO_f;
   end
elseif sum(r_FO_inside) == 0 % if none point of R foot in platform 
    RFO_f = RFO_f;
    if sum(l_FO_inside) == 4 % if all points of L foot in platform is validated
        LFO_f = [LFO_f, FP_FO_f];
    else % else none feet are validated
        LFO_f = [];
    end
else
    LFO_f = LFO_f;
    RFO_f = RFO_f;
end
end
