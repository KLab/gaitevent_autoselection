function REF = mean_refparameters(Foot_temp)
    
fields = fieldnames(Foot_temp);
fields = fields(1:length(fields)-4);
for f = 1:length(fields)
    m_temp = [];
    for s = 1:size(Foot_temp,2)
        m_temp = [m_temp; Foot_temp(s).(fields{f})];
    end
    REF.(fields{f}) = mean(m_temp);
    REF.([fields{f},'_std']) = std(m_temp);
end
end