function [ norm_vect ] = Normalize_vectors( vector)
%   MATH_SCALE_VALUES
%   Converts a value from one range into another
%       (maxNewRange - minNewRange)(originalValue - minOriginalRange)
%    y = ----------------------------------------------------------- + minNewRange      
%               (maxOriginalRange - minOriginalRange)
max_v = max(vector);
min_v = min(vector);
norm_vect = zeros(length(vector),1);
for i = 1:length(vector)
    norm_vect(i) = (vector(i)-min_v)/(max_v-min_v);
end
end