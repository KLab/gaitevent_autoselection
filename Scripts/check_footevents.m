function [fs, fo] = check_footevents(fs, fo, C3D)
%% 1. If no foot off consecutive to foot strike
if length(fs)>1     
delta_fs = fs(2)-fs(1);
    for i = 1:length(fs)-1
        f = abs(fo-fs(i));
        l = f < double(delta_fs*0.25);
        fo(l==1)=[];
    end
end

%% 2. If no missing foot strike 
if isempty(fs)
    disp('[Warning] No Heel Strike detected.')
    %%%%% ouvre mokka pour valider visuellement les cycles de marche
    system(['D:\Programme\Mokka\Mokka.exe -p "' C3D.pathname  C3D.filename '"']);
elseif fo(1) < fs(1) && fo(end)>fs(end)
    if abs(length(fs)-length(fo)) ~= 1
        disp('[Warning] Heel Strike not matching with foot off. Please correct')
        %%%%% ouvre mokka pour valider visuellement les cycles de marche
        system(['D:\Programme\Mokka\Mokka.exe -p "' C3D.pathname  C3D.filename '"']);
    end
elseif fo(1) < fs(1) && fo(end) < fs(end)
    if abs(length(fs) - length(fo)) ~= 0
        disp('[Warning] Heel Strike not matching with foot off. Please correct') 
        %%%%% ouvre mokka pour valider visuellement les cycles de marche
        system(['D:\Programme\Mokka\Mokka.exe -p "' C3D.pathname  C3D.filename '"']);
    end
end
end