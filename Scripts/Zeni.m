function C3D = Zeni(C3D,trial_values)
% Compute Events from kinematics data
% See paper  Zeni2008GP, Zeni -Two simple methods for determining
% gait events during treadmill and overground walking using kinematics data
% (modification of markers taken for the sacrum, modification to consider 
% the entire signal when performing findpeaks & correction of the events 
% time saved in c3d files by Marys Franco Carvalho in February 2022)

M={'LTOE';'LHEE';'RTOE';'RHEE'};
L={'LTOE';'LHEE'};
R={'RTOE';'RHEE'};
S={'SACR'};
values={'Events from Kinematics Detection Zeni et al 2008 GP'};
format='Char';
INFO = btkMetaDataInfo(format, values) ;
nb_frame=125; % minimum data with all the markers to detect events
mph=20; % minimum heigth for detect events
mpd=20; % distance minimum between 2 events

[b,a] = butter(4,5/(100/2),'low');%200/fs/2
wo = 50 / (100/2);
bw = wo / 50;
if(nargin == 1)
    trial_values = ones(length(C3D),1);
end

for n=1:length(C3D)
    if(trial_values(n) == 1)
        Mm=0;
        for m=1:length(M)
            if isfield(C3D(n).data,M{m})==1
                Mm=Mm+1;
            end
        end
        % take the marker SACR or the middle between markers LPSI & RPSI 
        % as sacrum (modified by Marys Franco Carvalho in February 2022)
        Sm=char(S);
        if isfield(C3D(n).data,'SACR')==1
            Mm=Mm+1;
        elseif isfield(C3D(n).data,'LPSI')==1 && isfield(C3D(n).data,'RPSI')==1
            C3D(n).data.(Sm)=(C3D(n).data.LPSI + C3D(n).data.RPSI)./2;
            Mm=Mm+1;
        end
        
        if Mm>=5
            A=0;duration=[];starts=[];
            for l=1:length(L)
                %detection presence marqueur gauche
                A=A+(C3D(n).data.(L{l})(:,1)==0);
                A=A*1;
            end
            A=A+(C3D(n).data.(Sm)(:,1)==0); % for marker pelvis
            runs = diff(find([1,diff(A'),1]));
            starts = find([1,diff(A')]);
            if length(starts)==1
                duration=length(A);
            else
                duration=diff(starts);
            end
            starts=[starts length(A)];
            C3D(n).EventFrame.Left_Foot_Strike_D=[];
            C3D(n).EventFrame.Left_Foot_Off_D=[];
            for s=1:length(starts)-1
                if A(starts(s))==0
                    if duration(s)>nb_frame
                        %detection sens marche
                        sens=mean(C3D(n).data.(Sm)(starts(s):starts(s)+10)-C3D(n).data.(Sm)(starts(s+1)-10:starts(s+1)));
                        if sens>0 % retour
                            ss=-1;
                        else
                            ss=1;
                        end

                        % detection foot strike
                        data=[];pks=[];locs=[];
                        % correction to take the entire signal (corrected by Marys Franco Carvalho in February 2022)
                        data=ss*(C3D(n).data.LHEE(starts(s):starts(s+1),1)-C3D(n).data.(Sm)(starts(s):starts(s+1),1));
                        data=filtfilt(b,a,data)-mean(data, 'omitnan');
                        [pks,locs] = findpeaks(data,'minpeakheight',mph,'minpeakdistance',mpd);
                        % correction of event frame adding -1 (corrected by Marys Franco Carvalho in February 2022)
                        C3D(n).EventFrame.Left_Foot_Strike_D=[C3D(n).EventFrame.Left_Foot_Strike_D locs+starts(s)-1];

                        % detection foot off
                        data=[];pks=[];locs=[];
                        % correction to take the entire signal (corrected by Marys Franco Carvalho in February 2022)
                        data=ss*(-C3D(n).data.LTOE(starts(s):starts(s+1),1)+C3D(n).data.(Sm)(starts(s):starts(s+1),1));
                        data=filtfilt(b,a,data)-mean(data, 'omitnan');
                        [pks,locs] = findpeaks(data,'minpeakheight',mph,'minpeakdistance',mpd);
                        % correction of event frame adding -1 (corrected by Marys Franco Carvalho in February 2022)
                        C3D(n).EventFrame.Left_Foot_Off_D=[C3D(n).EventFrame.Left_Foot_Off_D locs+starts(s)-1];
                    end
                end
            end
            A=0;duration=[];starts=[];
            for l=1:length(R)
                %detection presence marqueur droit
                A=A+(C3D(n).data.(R{l})(:,1)==0);
                A=A*1;
            end
            A=A+(C3D(n).data.(Sm)(:,1)==0); % for marker pelvis
            runs = diff(find([1,diff(A'),1]));
            starts = find([1,diff(A')]);
            if length(starts)==1
                duration=length(A);
            else
                duration=diff(starts);
            end
            starts=[starts length(A)];
            C3D(n).EventFrame.Right_Foot_Strike_D=[];
            C3D(n).EventFrame.Right_Foot_Off_D=[];
            for s=1:length(starts)-1
                if A(starts(s))==0
                    if duration(s)>nb_frame
                        %detection sens marche
                        sens=mean(C3D(n).data.(Sm)(starts(s):starts(s)+10)-C3D(n).data.(Sm)(starts(s+1)-10:starts(s+1)));
                        if sens>0 % retour
                            ss=-1;
                        else
                            ss=1;
                        end
                        
                        % detection foot strike
                        data=[];pks=[];locs=[];
                        % correction to take the entire signal (corrected by Marys Franco Carvalho in February 2022)
                        data=ss*(C3D(n).data.RHEE(starts(s):starts(s+1),1)-C3D(n).data.(Sm)(starts(s):starts(s+1),1));
                        data=filtfilt(b,a,data)-mean(data, 'omitnan');
                        [pks,locs] = findpeaks(data,'minpeakheight',mph,'minpeakdistance',mpd);
                        % correction of event frame adding -1 (corrected by Marys Franco Carvalho in February 2022)
                        C3D(n).EventFrame.Right_Foot_Strike_D=[C3D(n).EventFrame.Right_Foot_Strike_D locs+starts(s)-1];
                        
                        % detection foot off
                        data=[];pks=[];locs=[];
                        % correction to take the entire signal (corrected by Marys Franco Carvalho in February 2022)
                        data=ss*(-C3D(n).data.RTOE(starts(s):starts(s+1),1)+C3D(n).data.(Sm)(starts(s):starts(s+1),1));
                        data=filtfilt(b,a,data)-mean(data, 'omitnan');
                        [pks,locs] = findpeaks(data,'minpeakheight',mph,'minpeakdistance',mpd);
                        % correction of event frame adding -1 (corrected by Marys Franco Carvalho in February 2022)
                        C3D(n).EventFrame.Right_Foot_Off_D=[C3D(n).EventFrame.Right_Foot_Off_D locs+starts(s)-1];
                    end
                end
            end
        end
        btkClearEvents(C3D(n).acq);
        if isempty(C3D(n).EventFrame.Right_Foot_Strike_D)==0
            for c=1:length(C3D(n).EventFrame.Right_Foot_Strike_D)
                % correction in the temporal conversion (adding -1) (corrected by Marys Franco Carvalho in February 2022)
                btkAppendEvent(C3D(n).acq, 'Foot Strike', (C3D(n).EventFrame.Right_Foot_Strike_D(c)-1+C3D(n).StartFrame-1)/C3D(n).fRate.Point, 'Right','','',1);
            end
        end
        if isempty(C3D(n).EventFrame.Right_Foot_Off_D)==0
            for c=1:length(C3D(n).EventFrame.Right_Foot_Off_D)
                % correction in the temporal conversion (adding -1) (corrected by Marys Franco Carvalho in February 2022)
                btkAppendEvent(C3D(n).acq, 'Foot Off', (C3D(n).EventFrame.Right_Foot_Off_D(c)-1+C3D(n).StartFrame-1)/C3D(n).fRate.Point, 'Right','','',2);
            end
        end
        if isempty(C3D(n).EventFrame.Left_Foot_Strike_D)==0
            for c=1:length(C3D(n).EventFrame.Left_Foot_Strike_D)
                % correction in the temporal conversion (adding -1) (corrected by Marys Franco Carvalho in February 2022)
                btkAppendEvent(C3D(n).acq, 'Foot Strike', (C3D(n).EventFrame.Left_Foot_Strike_D(c)-1+C3D(n).StartFrame-1)/C3D(n).fRate.Point, 'Left','','',1);
            end
        end
        if isempty(C3D(n).EventFrame.Left_Foot_Off_D)==0
            for c=1:length(C3D(n).EventFrame.Left_Foot_Off_D)
                % correction in the temporal conversion (adding -1) (corrected by Marys Franco Carvalho in February 2022)
                btkAppendEvent(C3D(n).acq, 'Foot Off', (C3D(n).EventFrame.Left_Foot_Off_D(c)-1+C3D(n).StartFrame-1)/C3D(n).fRate.Point, 'Left','','',2);
            end
        end
      
        MM=btkAppendMetaData(C3D(n).acq, 'EVENT', 'Zeni et al.',INFO);%Ecrit dans C3D le filtre utilis?
        btkWriteAcquisition(C3D(n).acq,[C3D(n).pathname C3D(n).filename ]);
        disp(['Write ' C3D(n).filename ' with GaitRite events']);%
    end
%     if ~strcmp(C3D(n).filename(22),'S')
%         md = btkFindMetaData(C3D(n).acq,'CYCLES');
%         if ~isfield(md,'children') % exists
%             ev=btkGetEvents(C3D(n).acq);
%             if isfield(ev,'Left_Foot_Strike')
%                 InfoFrame.Left_cycle=ones(1,length(ev.Left_Foot_Strike)-1);
%                 %% Create the field CYCLE
%                 inf=num2str(InfoFrame.Left_cycle(1,:));
%                 INFO = btkMetaDataInfo('Char', {strrep(inf,'  ',',')});
%                 M=btkAppendMetaData(C3D(n).acq,'CYCLES','Left_cycle',INFO);
%                 btkWriteAcquisition(C3D(n).acq,[C3D(n).pathname C3D(n).filename ]);
%             end
%             if isfield(ev,'Right_Foot_Strike')
%                 InfoFrame.Right_cycle=ones(1,length(ev.Right_Foot_Strike)-1);
%                 inf=num2str(InfoFrame.Right_cycle(1,:));
%                 INFO = btkMetaDataInfo('Char', {strrep(inf,'  ',',')});
%                 M=btkAppendMetaData(C3D(n).acq,'CYCLES','Right_cycle',INFO);
%                 btkWriteAcquisition(C3D(n).acq,[C3D(n).pathname C3D(n).filename ]);
%             end
%         end
%     end
      %% Close c3d file        
%     btkDeleteAcquisition(C3D(n).acq); 
% btkCloseAcquisition(C3D(n).acq);
end

