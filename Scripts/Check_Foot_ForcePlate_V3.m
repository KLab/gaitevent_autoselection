function Foot =  Check_Foot_ForcePlate_V3(path, filename)
%check forceplates with order of feet in the platforms
%% 1. Get data
acq  = btkReadAcquisition([path, filename]);
m    = btkGetMarkers(acq);
ev   = btkGetEvents(acq);
fp  = btkGetForcePlatforms(acq);
frate = btkGetPointFrequency(acq);

%% 1.1. Count the number of force plates 

%% 2. Define force plates coordinates
if ~isempty(fp)
  
    %% 3. Check existence of the events and store frames
    
    events    = fieldnames(ev);
    InfoFrame.RHS = [];
    InfoFrame.LHS = [];
    InfoFrame.RFO = [];
    InfoFrame.LFO = [];

    Foot = [];
    for j=1:size(events,1)
        if any(~cellfun('isempty',strfind(events(j),'Right_Foot_Strike')))
            InfoFrame.RHS       = round((ev.(events{j}) * frate +1) - btkGetFirstFrame(acq));
            InfoFrame.RHSALL       = round((ev.(events{j}) * frate +1) - btkGetFirstFrame(acq));
        end
        if any(~cellfun('isempty',strfind(events(j),'Left_Foot_Strike')))
            InfoFrame.LHS       = round((ev.(events{j}) * frate +1) - btkGetFirstFrame(acq));
            InfoFrame.LHSALL       = round((ev.(events{j}) * frate +1) - btkGetFirstFrame(acq));
        end
        if any(~cellfun('isempty',strfind(events(j),'Right_Foot_Off')))
            InfoFrame.RFO       = round((ev.(events{j}) * frate +1) - btkGetFirstFrame(acq));
            InfoFrame.RFOALL       = round((ev.(events{j}) * frate +1) - btkGetFirstFrame(acq));
        end
        if any(~cellfun('isempty',strfind(events(j),'Left_Foot_Off')))
            InfoFrame.LFO       = round((ev.(events{j}) * frate +1) - btkGetFirstFrame(acq));
            InfoFrame.LFOALL       = round((ev.(events{j}) * frate +1) - btkGetFirstFrame(acq));
        end
    end
    
    
    %% 4. Delete incomplete cycles. If FOff comes before a HS or HS has not the respective FOff.
%     if ~isempty(ev([]))
    f = fieldnames(ev);
%     if ~isempty(ev.Left_Foot_Strike) == 1 && ~isempty(ev.Right_Foot_Strike) == 1
    if any(strcmp(f, 'Left_Foot_Strike'))== 1 && any(strcmp(f, 'Right_Foot_Strike'))== 1
        check = 1;
        if InfoFrame.RFO(1) < InfoFrame.RHS(1)
            InfoFrame.RFO(1) = [];
        end
        if InfoFrame.LFO(1) < InfoFrame.LHS(1)
            InfoFrame.LFO(1) = [];
        end
        if size(InfoFrame.RFO,2)<size(InfoFrame.RHS,2)
            InfoFrame.RHS(end) = [];
        end
        if size(InfoFrame.LFO,2)<size(InfoFrame.LHS,2)
            InfoFrame.LHS(end) = [];
        end
    else
        disp(['No events detected on file:', filename])
        check = 0;
    end
    
    if check == 1
        %% 5. Calculate dimensions of the feet
     
        size_frames = round(size(m.RTOE,1)/2);
        L = [m.LHEE(size_frames, 1), m.LHEE(size_frames, 2), m.LHEE(size_frames, 3); m.LTOE(size_frames, 1), m.LTOE(size_frames, 2), m.LTOE(size_frames, 3)]; % Marker coordinates at 50% of trial
        R = [m.RHEE(size_frames, 1), m.RHEE(size_frames, 2), m.RHEE(size_frames, 3); m.RTOE(size_frames, 1), m.RTOE(size_frames, 2), m.RTOE(size_frames, 3)];
        Length_LFoot = pdist(L, 'euclidean');
        Length_RFoot = pdist(R, 'euclidean');
        InfoFrame.Left_cycle=ones(1,length(ev.Left_Foot_Strike)-1);
        InfoFrame.Right_cycle=ones(1,length(ev.Right_Foot_Strike)-1);
        sense=-1;
        if length(fp)>1
              %% 6. Sense/direction of trial
              P1       = fp(1).corners;
              P2       = fp(2).corners;
              center_1 = (P1(1:2,1)/2 + P1(1:2,3)/2);
              center_2 = (P2(1:2,1)/2 + P2(1:2,3)/2);
              dist_P1 = pdist([m.C7(10,1), m.C7(10,2); center_1(1), center_1(2)], 'euclidean');
              dist_P2 = pdist([m.C7(10,1), m.C7(10,2); center_2(1), center_2(2)], 'euclidean');
              if dist_P1 < dist_P2
                  sense = 1;  %from plat 1 to 2
              elseif dist_P1 > dist_P2
                  sense = 2;   %from plat 2 to 1
              end
        end
        for p=1:length(fp)
            [InfoFrame] = Check_Foot_ForcePlate_1FP(fp(p),InfoFrame,Length_LFoot,Length_RFoot ,m,sense);
            
            if strcmp (InfoFrame.Left_P, 'Valid')
                Foot = [Foot,'L'];

            elseif strcmp (InfoFrame.Right_P, 'Valid')
                Foot = [Foot,'R'];
            else
                Foot = [Foot,'X'];
            end
            
        end
        %% Update MetaData
        % Field processing with information about method used
        INFO = btkMetaDataInfo('Char', {'Check_Foot_ForcePlate_V3'});
        M=btkAppendMetaData(acq,'PROCESSING','METHOD_Check_ForcePlate',INFO);
        INFO = btkMetaDataInfo('Char', {datestr(now,'yyyy-mm-dd')});
        M=btkAppendMetaData(acq,'PROCESSING','DATE_METHOD_Check_ForcePlate',INFO);
     
        
         % Field cycles with information about validity of cycles
         inf=num2str(InfoFrame.Left_cycle(1,:));
         INFO = btkMetaDataInfo('Char', {strrep(inf,'  ',',')});
         M=btkAppendMetaData(acq,'CYCLES','Left_cycle',INFO);
         inf=num2str(InfoFrame.Right_cycle(1,:));
         INFO = btkMetaDataInfo('Char', {strrep(inf,'  ',',')});
         M=btkAppendMetaData(acq,'CYCLES','Right_cycle',INFO);
         btkWriteAcquisition(acq,[path, filename]);
    else
        for p=1:length(fp)
        Foot = [Foot,'X'];
        end
    end
    
  btkCloseAcquisition(acq);
else
    nm=fieldnames(m);
    btkAppendForcePlatformType2(acq, ones(length(m.(strcat(nm{1}))),3), ones(length(m.(strcat(nm{1}))),3), ones(4,3));
    btkAppendForcePlatformType2(acq, ones(length(m.(strcat(nm{1}))),3), ones(length(m.(strcat(nm{1}))),3), ones(4,3)) ;
%     Foot = 'XXX';
    btkWriteAcquisition(acq,[path, filename]);
    btkCloseAcquisition(acq);
end

