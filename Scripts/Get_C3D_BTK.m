function C3D = Get_C3D_BTK(C3D_path,C3D_filename,analog)
% to complete

warning off;
file = [C3D_path C3D_filename];
C3D.filename = C3D_filename;
C3D.pathname = C3D_path;
acq=btkReadAcquisition(strcat(C3D_path,C3D_filename));%'\',
md = btkFindMetaData(acq, 'MANUFACTURER', 'COMPANY');
if isstruct(md)==1
    MANUFACTURER=char(md.info.values);
    C3D.Manufacturer=MANUFACTURER;
else
    MANUFACTURER='Vicon';
end

C3D.acq=acq;
C3D.StartFrame = btkGetFirstFrame(acq);
C3D.EndFrame = btkGetLastFrame(acq);
C3D.fRate.Point = btkGetPointFrequency(acq);
C3D.fRate.Analog = btkGetAnalogFrequency(acq);
EventFrame=struct();
E=btkGetEvents(acq);
if ~isempty(fieldnames(E))
    a=fieldnames(E);
    if isempty(a)==0
        for i=1:length(a)
            Event.(char(a{i}))=sort(E.(char(a{i})));
            EventFrame.(char(a{i}))=round(Event.(char(a{i}))*C3D.fRate.Point+1-single(C3D.StartFrame)+1);%+1 pour la convertion temporelle  et +1 pour la diff?rence entre frame
            for j=1:length( EventFrame.(char(a{i})))
                if    EventFrame.(char(a{i}))(j)<1
                    EventFrame.(char(a{i}))(j)=1;
                end
            end
        end
        C3D.Event=Event;
        C3D.EventFrame=EventFrame;
    end
end
% %%%%%%%%%%[C3D.Spatio_temp.data, C3D.Spatio_temp.info]=btkGetAnalysis(acq);

% Pour vieux fichier que Nexus ne relie pas correctement - fr?quence:120Hz
% au lieu de 50Hz
% Correction des donn?es spatio-temp
var_name={'Left_Cadence','Left_Walking_Speed','Right_Walking_Speed','Right_Cadence'};
if C3D.fRate.Point==120
    if isfield(C3D,'Spatio_temp')==1
        if isfield(C3D.Spatio_temp,'data')==1
            for i=1:length(var_name)
                if isfield(C3D.Spatio_temp.data,var_name{i})==1
                    C3D.Spatio_temp.data.(var_name{i})=C3D.Spatio_temp.data.(var_name{i})/2.4;
                end
            end
        end
    end
end

temp=btkGetMetaData(acq);
C3D.MetaData=temp;
a=struct();
% Processing
Process=0;
if isfield(temp.children,'PROCESSING')==1
    a=fieldnames(temp.children.PROCESSING.children);
    for i=1:length(a)
        C3D.SubjectParam.(a{i})=temp.children.PROCESSING.children.(a{i}).info.values;
    end
    if isfield(C3D.SubjectParam,'BodyMass')==1
        Process=1;
    end
    
end
Subject=0;
if isfield(temp.children,'SUBJECTS')==1
    Subject=1;
    if isfield(temp.children.SUBJECTS.children, 'NAMES') == 1
        C3D.SubjectParam.name=temp.children.SUBJECTS.children.NAMES.info.values;
    else
        C3D.SubjectParam.name = 'NaN';
    end
%     C3D.SubjectParam.marker_set=temp.children.SUBJECTS.children.MARKER_SETS.info.values;
end

% if strcmp(MANUFACTURER,'Qualisys')
%     if isfield(temp.children,'ANALYSIS')==1
%         if isfield(temp.children.ANALYSIS.children,'VALUES')==1
%             VAL=temp.children.ANALYSIS.children.VALUES.info.values;
%             Names=temp.children.ANALYSIS.children.NAMES.info.values;
%             for i=1:length(VAL)
%                 C3D.SubjectParam.(Names{i})=VAL(i);
%             end
%         end
%     end
% end

Loc_str=[];
Loc_str=findstr('\', C3D_path);
[C3D.data C3D.dataInfo]=btkGetPoints(acq);
name=fieldnames(C3D.data);

if analog==1
    C3D.analog=btkGetAnalogs(acq);
end

C3D.Patient_Name=C3D_path(Loc_str(end-2)+1:Loc_str(end-1)-1);
C3D.Session_Name=C3D_path(Loc_str(end-1)+1:Loc_str(end)-1);

if length(C3D_filename)==26
    if strcmp(C3D_filename(end-6),'1')==1
        file_num=str2num(C3D_filename(end-6:end-4));
    else
        file_num=str2num(C3D_filename(end-5:end-4));
    end

elseif length(C3D_filename)==27
    file_num=str2num(C3D_filename(end-6:end-4));
else
    file_num = str2num(C3D_filename(end-6:end-4));
end


if strcmp(MANUFACTURER,'Qualisys')
    enf_file=[C3D_path C3D_filename(1:end-4) '.qtf'];
else
    enf_file=[C3D_path C3D_filename(1:end-4) '.Trial' num2str(file_num) '.enf'];
end
a=fopen(enf_file);
file_num=0;


while a<0
    file_num=file_num+1;
    if file_num<10
        enf_file=[C3D_path C3D_filename(1:end-4) '.Trial0' num2str(file_num) '.enf'];
    else
        enf_file=[C3D_path C3D_filename(1:end-4) '.Trial' num2str(file_num) '.enf'];
    end
    a=fopen(enf_file);
    if file_num==150
        a=0;
    end
end

if a>0
    [pathstr,name,ext] = fileparts(enf_file);
    C3D.Enf_file=[name ext];
    C3D.Trial_info=Get_Enf_General(enf_file);
    if isfield(C3D,'Event')
        if isfield(C3D.Event,'Right_Foot_Strike')
            if isfield(C3D.Trial_info,'RCycle')
                C3D.RCycle= C3D.Trial_info.RCycle;
                
                if length(C3D.RCycle)==length(C3D.Event.Right_Foot_Strike)-1
                    C3D.Valid.R =1;
                else disp('Probleme de correspondance dans les cycles selectionnes a droite');
                end
                
            end
        end
        if isfield(C3D.Event,'Left_Foot_Strike')
            if isfield(C3D.Trial_info,'LCycle')
                C3D.LCycle= C3D.Trial_info.LCycle;
                if length(C3D.LCycle)==length(C3D.Event.Left_Foot_Strike)-1
                    C3D.Valid.L =1;
                else disp('Probleme de correspondance dans les cycles selectionnes a gauche');
                end
            end
        end
    end
end

d=struct();
d=dir(C3D_path);
if  Subject==1 && Process==0;
    passe=0;
    for i=1:length(d)
        if strcmpi(strcat(C3D.SubjectParam.name,'.mp'),d(i).name)==1 % '.' C3D_filename(1:end-4) '.enf'
            C3D=get_mp(C3D, char(d(i).name));
            passe=1;
        end
    end
%     if passe==0
%         disp('No mp file');
%     end
end
%btkDeleteAcquisition(C3D.acq)
fclose('all');
clear acq;