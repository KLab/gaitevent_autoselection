function [LFS_f,LFO_f,RFS_f,RFO_f] = Check_Foot_ForcePlate_GE(fp_corners, m ,sense,foot_wide_threshold,foot_front_threshold, FP_FS_f, FP_FO_f,ff)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%% FOOT STRIKE
RHEE_FS = m.RHEE(FP_FS_f-ff,:);                        % Get the coordinates of foot markers on the frame of interest
LHEE_FS = m.LHEE(FP_FS_f-ff,:);
RTOE_FS = m.RTOE(FP_FS_f-ff,:); 
LTOE_FS = m.LTOE(FP_FS_f-ff,:); 

% initialize variables
LFS_f = [];
RFS_f = [];
LFO_f = [];
RFO_f = [];

% size of the feet at foot strike (x component)
% L = [LHEE_FS(1,1),LHEE_FS(1,2), LHEE_FS(1,3); LTOE_FS(1, 1), LTOE_FS(1,2), LTOE_FS(1,3)];
% R = [RHEE_FS(1,1),RHEE_FS(1,2), RHEE_FS(1,3); RTOE_FS(1, 1), RTOE_FS(1,2), RTOE_FS(1,3)];
% Length_LFoot = pdist(L, 'euclidean');
% Length_RFoot = pdist(R, 'euclidean');
Length_LFoot = abs(LHEE_FS(1,1)-LTOE_FS(1,1))*foot_front_threshold;
Length_RFoot = abs(RHEE_FS(1,1)-RTOE_FS(1,1))*foot_front_threshold;
Left_width = (foot_wide_threshold*Length_LFoot)/2;
Right_width = (foot_wide_threshold*Length_RFoot)/2;
 
if sense == 1                                          % direction platform 1 -> 2
    lf_corners  = [LHEE_FS(1), LHEE_FS(2)-Left_width; LHEE_FS(1)+Length_LFoot, LHEE_FS(2)-Left_width;LHEE_FS(1)+Length_LFoot, LHEE_FS(2)+Left_width; LHEE_FS(1), LHEE_FS(2)+Left_width];
    rf_corners  = [RHEE_FS(1), RHEE_FS(2)-Right_width; RHEE_FS(1)+Length_RFoot, RHEE_FS(2)-Right_width;RHEE_FS(1)+Length_RFoot, RHEE_FS(2)+Right_width; RHEE_FS(1), RHEE_FS(2)+Right_width];
else                                                   % direction platform 2 -> 1
    lf_corners  = [LHEE_FS(1), LHEE_FS(2)+Left_width; LHEE_FS(1)-Length_LFoot, LHEE_FS(2)+Left_width;LHEE_FS(1)-Length_LFoot, LHEE_FS(2)-Left_width; LHEE_FS(1), LHEE_FS(2)-Left_width];
    rf_corners  = [RHEE_FS(1), RHEE_FS(2)+Right_width; RHEE_FS(1)-Length_RFoot, RHEE_FS(2)+Right_width;RHEE_FS(1)-Length_RFoot, RHEE_FS(2)-Right_width; RHEE_FS(1), RHEE_FS(2)-Right_width];
end

r_FS_inside = insidePlatform(rf_corners, fp_corners);
l_FS_inside = insidePlatform(lf_corners, fp_corners);

if sum(r_FS_inside) == 4 %if all points of R foot in
   if sum(l_FS_inside) > 0 % if at least one point of L foot in both feet are invalid
     RFS_f = RFS_f;
     LFS_f = LFS_f;
   else % if no point of L foot in then R foot in platform is validated
     RFS_f = [RFS_f, FP_FS_f];
     LFS_f = LFS_f;
   end
elseif sum(r_FS_inside) == 0 % if none point of R foot in platform 
    RFS_f = RFS_f;
    if sum(l_FS_inside) == 4 % if all points of L foot in platform is validated
        LFS_f = [LFS_f, FP_FS_f];
    else % else none feet are validated
        LFS_f = LFS_f;
    end
else 
    RFS_f = [];
    LFS_f = LFS_f;
end
    
%% FOOT OFF
RHEE_FO = m.RHEE(FP_FO_f-ff,:);                        % Get the coordinates of foot markers on the frame of interest
LHEE_FO = m.LHEE(FP_FO_f-ff,:);
RTOE_FO = m.RTOE(FP_FO_f-ff,:); 
LTOE_FO = m.LTOE(FP_FO_f-ff,:); 
 
% size of the feet at foot off (x component)
%  L = [LHEE_FO(1,1),LHEE_FO(1,2), LHEE_FO(1,3); LTOE_FO(1, 1), LTOE_FO(1,2), LTOE_FO(1,3)];
%  R = [RHEE_FO(1,1),RHEE_FO(1,2), RHEE_FO(1,3); RTOE_FO(1, 1), RTOE_FO(1,2), RTOE_FO(1,3)];
%  Length_LFoot = pdist(L, 'euclidean');
%  Length_RFoot = pdist(R, 'euclidean');
 Length_LFoot = abs(LHEE_FO(1,1)-LTOE_FO(1,1))*foot_front_threshold;
 Length_RFoot = abs(RHEE_FO(1,1)-RTOE_FO(1,1))*foot_front_threshold;
if sense == 1     % direction platform 1 -> 2
    lf_corners  = [LHEE_FO(1), LHEE_FO(2)-Left_width; LHEE_FO(1)+Length_LFoot, LHEE_FO(2)-Left_width;LHEE_FO(1)+Length_LFoot, LHEE_FO(2)+Left_width; LHEE_FO(1), LHEE_FO(2)+Left_width];
    rf_corners  = [RHEE_FO(1), RHEE_FO(2)-Right_width; RHEE_FO(1)+Length_RFoot, RHEE_FO(2)-Right_width;RHEE_FO(1)+Length_RFoot, RHEE_FO(2)+Right_width; RHEE_FO(1), RHEE_FO(2)+Right_width];
elseif sense == 2 % direction platform 2 -> 1
    lf_corners  = [LHEE_FO(1), LHEE_FO(2)+Left_width; LHEE_FO(1)-Length_LFoot, LHEE_FO(2)+Left_width;LHEE_FO(1)-Length_LFoot, LHEE_FO(2)-Left_width; LHEE_FO(1), LHEE_FO(2)-Left_width];
    rf_corners  = [RHEE_FO(1), RHEE_FO(2)+Right_width; RHEE_FO(1)-Length_RFoot, RHEE_FO(2)+Right_width;RHEE_FO(1)-Length_RFoot, RHEE_FO(2)-Right_width; RHEE_FO(1), RHEE_FO(2)-Right_width];
end


r_FO_inside = insidePlatform(rf_corners, fp_corners);
l_FO_inside = insidePlatform(lf_corners, fp_corners);

if sum(r_FO_inside) == 4 %if all points of R foot in
   if sum(l_FO_inside) > 0 % if at least one point of L foot in both feet are invalid
     RFO_f = RFO_f;
     LFO_f = LFO_f;
   else % if no point of L foot in then R foot in platform is validated
     RFO_f = [RFO_f,FP_FO_f];
     LFO_f = LFO_f;
   end
elseif sum(r_FO_inside) == 0 % if none point of R foot in platform 
    RFO_f = RFO_f;
    if sum(l_FO_inside) == 4 % if all points of L foot in platform is validated
        LFO_f = [LFO_f, FP_FO_f];
    else % else none feet are validated
        LFO_f = [];
    end
else
    LFO_f = LFO_f;
    RFO_f = RFO_f;
end
end

