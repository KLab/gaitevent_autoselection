function [IC_L, IC_R, TO_L, TO_R] = Ghoussayni(C3D)

IC_MarkerName_L = 'LHEE';
TO_MarkerName_L = 'LTOE';
IC_MarkerName_R = 'RHEE';
TO_MarkerName_R = 'RTOE';
Velo_MarkerName = 'SACR';

c3dfile = C3D.filename;
c3d_name = split(c3dfile,'.');

% Load c3d with btl
btkData=C3D.acq;
metadata=C3D.MetaData;
ff=C3D.StartFrame;
Markers = C3D.data;
f = C3D.fRate.Point;
freq=f;
n = length(Markers.LHEE());
if ~isfield(Markers, 'SACR')
    Markers.SACR = (Markers.LPSI' + Markers.RPSI').'/2;
end
%% Correct for Walking Direction
Velo_Marker = Markers.(Velo_MarkerName);

% delete zeros at the beginning or end of an trial
dir_i = abs(Velo_Marker(end, 1) - Velo_Marker(1, 1));
dir_j = abs(Velo_Marker(end, 2) - Velo_Marker(1, 2));

walkdir = 1;  % x is walkdir

if (dir_i < dir_j)
    walkdir = 2;  % y is walkdir
end

% pos. or neg. direktion on axis
sgn = sign(Velo_Marker(end, walkdir) - Velo_Marker(1, walkdir));
walkdir = walkdir * sgn;
[Markers_Corrected]=f_rotCoordinateSystem(Markers, walkdir, 1);
gaitAxis=1;
verticalAxis=3;

%% Filtering Markers and preprocessing
[B,A] = butter(4,6/(f/2),'low');
filt_IC_marker_L(:,:,1) = filtfilt(B, A, Markers_Corrected.(IC_MarkerName_L));
filt_TO_marker_L(:,:,1) = filtfilt(B, A, Markers_Corrected.(TO_MarkerName_L));
filt_IC_marker_R(:,:,1) = filtfilt(B, A, Markers_Corrected.(IC_MarkerName_R));
filt_TO_marker_R(:,:,1) = filtfilt(B, A, Markers_Corrected.(TO_MarkerName_R));
filt_Velo_marker(:,:,1) = filtfilt(B, A, Markers_Corrected.(Velo_MarkerName)); 
 
 y_velo=filt_Velo_marker(:,gaitAxis,:);
 z_velo=filt_Velo_marker(:,verticalAxis,:);
 
 %Determine approximate walking speed
 [vel,time]=f_approxVelocity(y_velo,z_velo,f);
 vel2=vel/100;
%% Kinematic Algorithm_Ghoussayni - initial contact
[IC_L,~] = f_Ghoussayni_500(filt_IC_marker_L,filt_TO_marker_L,gaitAxis,verticalAxis,n,f);
[IC_R,~] = f_Ghoussayni_500(filt_IC_marker_R,filt_TO_marker_R,gaitAxis,verticalAxis,n,f);

%% Kinematic Algorithm_ModifiedGhoussayni - toe-off
[~,TO_L]=f_Ghoussayni_variablethreshold(filt_IC_marker_L,filt_TO_marker_L,gaitAxis,verticalAxis,n,f,vel2);
[~,TO_R]=f_Ghoussayni_variablethreshold(filt_IC_marker_R,filt_TO_marker_R,gaitAxis,verticalAxis,n,f,vel2);
end