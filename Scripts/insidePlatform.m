function inside = insidePlatform (corners, Platform)
% Check if a rectangle is inside another.
% Inside counts the number of points inside the platform. 
% (sum(inside) = 4 if all 4 points are inside. 
%
% input:
% corners - foot corners (3x4)
% Platform - FP corners (3x4)

% output:
% inside - check list (1x4)

inside = zeros(4,1);

for i = 1:4
    if Platform(1,1) < corners(i,1) && corners(i,1) < Platform(1,2)
        if Platform(2,3) < corners(i,2) && corners(i,2) < Platform(2,2)
            inside(i) = 1;
        end
    else
        inside(i) = 0;
    end
end