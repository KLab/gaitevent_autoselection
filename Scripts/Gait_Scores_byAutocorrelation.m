% Gait event detection by autocorrelation between valid gait events in
% forceplate with rest of cycles of one session
% created by M.Fonseca September 2020

%% 1. Load list of files for one session
% get list of .c3d files
C3D_path_defaut='C:\Users\mcdf\OneDrive - HOPITAUX UNIVERSITAIRES DE GENEVE\Gitlab\Gait Events\Data\test\';
[C3D_filename,C3D_path,FilterIndex]=uigetfile({'*.c3d'},'Sélectionner les ficihers C3D ',C3D_path_defaut,'MultiSelect','on');
% define parameters
threshold_FP_detection = 20;
foot_wide_threshold = 1.2; % 1.2 correspond to an increment of 20% of foot width
foot_front_threshold = 1.1;
% ratio_length_wide = 0.45; % 0.4 = wide of foot is 45% of total length of the foot
toe_v_threshold = 1000;
% read C3D
% initiate foot coordinate Variables
Foot = struct('LHEE_FS',[], 'LHEE_FO',[], 'LTOE_FS',[], 'LTOE_FO',[],'RHEE_FS',[],...
    'RHEE_FO',[], 'RTOE_FS',[], 'RTOE_FO',[], 'LHEE_cycle_z',[],'LTOE_cycle_z',[],...
    'RHEE_cycle_z',[],'RTOE_cycle_z',[],'SACR_LFS',[],'SACR_RFS',[],'SACR_RFO',[],...
    'SACR_LFO',[]);
Check_EvFr = struct('LFS',[], 'LFO',[], 'RFS',[],'RFO',[]);
rfo_err = []; rfs_err = []; lfo_err = []; lfs_err = [];
values={'Events from Kinematics with autocorrelation with forceplate'};
format='Char';
for n=1:length(C3D_filename)
    C3D = Get_C3D_BTK(C3D_path,C3D_filename{n},0);
    disp(C3D_filename{n})
    %% 2. Check valid events in forceplate
    % detect gait events (foot strike + footoff) for both sides
    [Foot, Check_EvFr(n)] =  Check_Foot_ForcePlate_V4(C3D_path, C3D_filename{n}, threshold_FP_detection, Foot, foot_front_threshold, foot_wide_threshold);   
end

%% Calculate foot angle (LFS) and SACR-LHEE dist
ThetaInDegrees_lfs = CalculateAngle_FootFloor(Foot.LHEE_FS, Foot.LTOE_FS);
ThetaInDegrees_lfo = CalculateAngle_FootFloor(Foot.LHEE_FO, Foot.LTOE_FO);
ThetaInDegrees_rfs = CalculateAngle_FootFloor(Foot.RHEE_FS, Foot.RTOE_FS);
ThetaInDegrees_rfo = CalculateAngle_FootFloor(Foot.RHEE_FO, Foot.RTOE_FO);
FA_lfs = Foot.LTOE_FS(:,3)-Foot.LHEE_FS(:,3);
FA_rfs = Foot.RTOE_FS(:,3)-Foot.RHEE_FS(:,3);


%% Calculate x distance between SACR and HEE at Foot Strike
SACR_LHEE_x_hs = Foot.SACR_LFS(:,1) - Foot.LHEE_FS(:,1);
SACR_RHEE_x_hs = Foot.SACR_RFS(:,1) - Foot.RHEE_FS(:,1);

%% Calculate x distance between SACR and TOE at Foot Off
SACR_LTOE_x_fo = Foot.SACR_LFS(:,1) - Foot.LTOE_FS(:,1);
SACR_RTOE_x_fo = Foot.SACR_RFS(:,1) - Foot.RTOE_FS(:,1);

%% Display number of valid events detected
disp('-----------------------------------------------------------------------')
disp('Calibration events finalized')
disp(['Left side - Number of valid events detected: ', num2str(length(ThetaInDegrees_lfs))])
disp(['Right side - Number of valid events detected: ', num2str(length(ThetaInDegrees_rfs))])
disp('                                                                       ')
disp('-----------------------------------------------------------------------')

%% 4. Average/SD of all collected cycles relative to valid gait events
theta_LFS_m  = mean(ThetaInDegrees_lfs); % mean left foot angle w.r.t. floor at foot strike
LHEE_FS_m_z  = mean(Foot.LHEE_FS(:,3));  % mean LHEE in z at foot strike
LTOE_FS_m_z  = mean(Foot.LTOE_FS(:,3));  % mean LTOE in z at foot strike
theta_LFO_m  = mean(ThetaInDegrees_lfo); % mean left foot angle w.r.t. floot at foot off
LHEE_FO_m_z  = mean(Foot.LHEE_FO(:,3));  % mean LHEE in z at foot off
LTOE_FO_m_z  = mean(Foot.LTOE_FO(:,3));  % mean LTOE in z at foot off
SACR_LHEE_FS_m_x = mean(SACR_LHEE_x_hs); % mean x dist SACR-LHEE at foot strike
SACR_LTOE_FO_m_x = mean(SACR_LTOE_x_fo); % mean x dist SACR-LTOE at foot off
FA_lfs_m =  mean(FA_lfs);
% Calculate standard deviation, if only 1 trial sd is set by default
if size(Foot.LHEE_FS,1)<2
    theta_LFS_sd = theta_LFS_m/16;
    LHEE_FS_sd_z = LHEE_FS_m_z/16;
    LTOE_FS_sd   = LTOE_FS_m_z/16;  
    theta_LFO_sd = theta_LFO_m/16;
    LHEE_FO_sd_z  = LHEE_FO_m_z/16;
    LTOE_FO_z_sd = LTOE_FO_m_z/16;
    SACR_LHEE_FS_sd_x = SACR_LHEE_FS_m_x/16; % to correct
    SACR_LTOE_FO_sd_x = SACR_LTOE_FO_m_x/16 % to verify
elseif size(Foot.LHEE_FS,1)>1
    theta_LFS_sd = std(ThetaInDegrees_lfs);
    LHEE_FS_sd_z = std(Foot.LHEE_FS(:,3));
    LTOE_FS_sd   = std(Foot.LTOE_FS(:,3));
    theta_LFO_sd = std(ThetaInDegrees_lfo);
    LHEE_FO_sd_z = std(Foot.LHEE_FO(:,3));
    LTOE_FO_z_sd = std(Foot.LTOE_FO(:,3));
    SACR_LHEE_FS_sd_x = std(SACR_LHEE_x_hs);
    SACR_RTOE_FO_sd_x = std(SACR_LTOE_x_fo);
end

theta_RFS_m  = mean(ThetaInDegrees_rfs); % mean right foot angle w.r.t. floor at foot strike
RHEE_FS_m_z  = mean(Foot.RHEE_FS(:,3));  % mean RHEE in z at foot strike
theta_RFO_m  = mean(ThetaInDegrees_rfo); % mean RTOE in z at foot strike
RHEE_FO_m_z  = mean(Foot.RHEE_FO(:,3));  % mean right foot angle w.r.t. floot at foot off
RTOE_FS_m_z  = mean(Foot.RTOE_FS(:,3));  % mean RHEE in z at foot off
RTOE_FO_m_z  = mean(Foot.RTOE_FO(:,3));  % mean RTOE in z at foot off
SACR_RHEE_FS_m_x = mean(SACR_RHEE_x_hs); % mean x dist SACR-RHEE at foot strike
SACR_RTOE_FO_m_x = mean(SACR_RTOE_x_fo);
FA_rfs_m = mean(FA_rfs);
% Calculate standard deviation, if only 1 trial sd is set by default
if size(Foot.RHEE_FS,1)<2
    theta_RFS_sd = theta_RFS_m/16;
    RHEE_FS_sd_z = RHEE_FS_m_z/16;
    RHEE_FO_sd_z = RHEE_FO_m_z/16;
    RTOE_FS_sd   = RTOE_FS_m_z/16;  
    theta_RFO_sd = theta_RFO_m/16;
    RTOE_FO_sd = std_default;
    SACR_RHEE_FS_m_x = SACR_RHEE_FS_m_x/16; % to verify
    SACR_RTOE_FO_m_x = SACR_RTOE_FO_m_x/16 % to verify
elseif size(Foot.RHEE_FS,1)>1
    theta_RFS_sd = std(ThetaInDegrees_rfs);
    RHEE_FS_sd_z = std(Foot.RHEE_FS(:,3));
    RHEE_FO_sd_z = std(Foot.RHEE_FO(:,3));
    RTOE_FS_sd   = std(Foot.RTOE_FS(:,3));
    theta_RFO_sd = std(ThetaInDegrees_rfo);
    RTOE_FO_sd = std(Foot.RTOE_FO(:,3));
    SACR_RHEE_FS_m_x = std(SACR_RHEE_x_hs);
    SACR_RTOE_FO_m_x = std(SACR_RTOE_x_fo);
end

%% 5. Find same positions in all trials and write gait events on c3d
% open c3d and check for frames matching 
for n=1:length(C3D_filename)
    C3D = Get_C3D_BTK(C3D_path,C3D_filename{n},0);
    %% 2. Check valid events in forceplate
    % detect gait events (foot strike + footoff) for both sides
    acq=btkReadAcquisition(strcat(C3D_path,C3D_filename{n}));%'\'
    m    = btkGetMarkers(acq);
    fp  = btkGetForcePlatforms(acq);
    frate = btkGetPointFrequency(acq);
    metadata = btkGetMetaData(acq);
    ff = btkGetFirstFrame(acq);
    nframes = length(m.LHEE(:,1));
    filterMarkerTrajectoryPyCGM(C3D, 4, 6,{'LHEE','LTOE','RHEE','RTOE','SACR'});

    % Sense
    if m.SACR(1,1)>m.SACR(end,1)
        sense = -1; % right to left
    else
        sense = 1; % left to right
    end
    % calculate mean of parameters used in trial (for normalization)
    mean_LHEE = mean(m.LHEE(:,:));
    mean_LTOE = mean(m.LTOE(:,:));
    mean_RHEE = mean(m.RHEE(:,:));
    mean_RTOE = mean(m.RTOE(:,:));
    mean_SACR = mean(m.SACR(:,:));
    
    % Height of heel HS (normalised)
    LHEE_fs_c = m.LHEE(:,3) - mean_LHEE(3) - LHEE_FS_m_z;
    LTOE_fs_c = m.LTOE(:,3) - mean_LTOE(3) - LTOE_FS_m_z;
    RHEE_fs_c = m.RHEE(:,3) - mean_RHEE(3) - RHEE_FS_m_z;
    RTOE_fs_c = m.RTOE(:,3) - mean_RTOE(3) - RTOE_FS_m_z;
    
    % Height of heel FO 
    LHEE_fo_c = m.LHEE(:,3) - mean_LHEE(3) - LHEE_FO_m_z;
    LTOE_fo_c = m.LTOE(:,3) - mean_LTOE(3) - LTOE_FO_m_z;
    RHEE_fo_c = m.RHEE(:,3) - mean_RHEE(3) - RHEE_FO_m_z;
    RTOE_fo_c = m.RTOE(:,3) - mean_RTOE(3) - RTOE_FO_m_z;
    
    % SACR-HEE_x HS
    SACR_LHEE_x = m.SACR(:,1)-m.LHEE(:,1);
    SACR_RHEE_x = m.SACR(:,1)-m.RHEE(:,1);
    %SACR-TOE_x FO
    SACR_LTOE_x = (m.SACR(:,1)-m.LTOE(:,1))*sense;
    SACR_RTOE_x = (m.SACR(:,1)-m.RTOE(:,1))*sense;
    
    % angle foot during trial
    LFAngle = CalculateAngle_FootFloor(m.LHEE,m.LTOE);
    RFAngle = CalculateAngle_FootFloor(m.RHEE,m.RTOE);
    LFA = m.LTOE(:,3)-m.LHEE(:,3);
    RFA = m.RTOE(:,3)-m.RHEE(:,3);
    % difference between angle and angle at frame event
    Ldiff_Angle_fs = LFAngle-theta_LFS_m;
    Rdiff_Angle_fs = RFAngle-theta_RFS_m;
	Ldiff_Angle_fo = LFAngle-theta_LFO_m;
    Rdiff_Angle_fo = RFAngle-theta_RFO_m;
    diffLFA_fs = LFA-FA_lfs_m;
    diffRFA_fs = RFA-FA_rfs_m;
    
    LHS = zeros(size(m.LHEE(:,3),1),1);
    RHS = zeros(size(m.RHEE(:,3),1),1);
    LFO = zeros(size(m.LHEE(:,3),1),1);
    RFO = zeros(size(m.RHEE(:,3),1),1);
    disp(C3D_filename{n})
    
    for f = 1 :nframes-1     
       if abs(LTOE_fs_c(f)) < abs(5)
          LHS(f)= LHS(f)+2; 
       end        

       if abs(RTOE_fs_c(f)) < abs(5)
          RHS(f)= RHS(f)+2; 
       end  
% % %        
       % condition LHEEz
       if abs(LHEE_fo_c(f)) < abs(LHEE_FO_sd_z)
           LFO(f)= LFO(f)+1;
       end      
%        % condition foot angle
       if abs(Ldiff_Angle_fo(f)) < abs(theta_LFO_sd)
          LFO(f)= LFO(f)+1;
       end
       if (LTOE_fo_c(f)) < 0 && (LTOE_fo_c(f+1))>0
          LFO(f)= LFO(f+1)+1; 
       end       
       
       % condition RHEEz
       if abs(RHEE_fo_c(f)) < abs(RHEE_FO_sd_z)
           RFO(f)= RFO(f)+2;
       end      
       % condition foot angle
       if abs(Rdiff_Angle_fo(f)) < abs(theta_RFO_sd)
          RFO(f)= RFO(f)+1;
       end
       if RTOE_fo_c(f) < 0 && RTOE_fo_c(f+1)>0
          RFO(f)= RFO(f+1)+1; 
       end
    end
    
    %% condition LHEE velocity
    t = 1:nframes;
    LHEE_v_z = zeros(length(t)-1, 1);
    RHEE_v_z = zeros(length(t)-1, 1);
    SACR_LHEE_x_v=zeros(length(t)-1,1);
    SACR_RHEE_x_v=zeros(length(t)-1,1);
    LTOE_v_x = zeros(length(t)-1,1);
    RTOE_v_x = zeros(length(t)-1,1);
    for i=1:length(t)-1
        LHEE_v_z(i) = ((m.LHEE(i+1,1)-m.LHEE(i,1))*frate)*sense;
        SACR_LHEE_x_v(i) = -((SACR_LHEE_x(i+1)-SACR_LHEE_x(i))*frate)*sense;
        RHEE_v_z(i) = ((m.RHEE(i+1,1)-m.RHEE(i,1))*frate)*sense;
        SACR_RHEE_x_v(i) = -((SACR_RHEE_x(i+1)-SACR_RHEE_x(i))*frate)*sense;
        LTOE_v_x(i)=((m.LTOE(i+1,1)-m.LTOE(i,1))*frate)*sense;
        RTOE_v_x(i)=((m.RTOE(i+1,1)-m.RTOE(i,1))*frate)*sense;
    end

    LHEE_v_z(1)=0;SACR_LHEE_x_v(1)=0;RHEE_v_z(1)=0;SACR_RHEE_x_v(1)=0;LTOE_v_x(1)=0;
    
    % condition velocity in x of TOE
    LTOE_v_x(LTOE_v_x < toe_v_threshold)= 0;
    RTOE_v_x(RTOE_v_x < toe_v_threshold)= 0;
    [pks, locs] = findpeaks(diff(LTOE_v_x),'MinPeakHeight', 800, 'MinPeakDistance',double(nframes*0.2));
    for LL = 1:length(locs)
        LFO(locs(LL)) = LFO(locs(LL)-1)+2;
    end
    [pks, locs] = findpeaks(diff(RTOE_v_x),'MinPeakHeight', 800, 'MinPeakDistance',double(nframes*0.2));
    for LL = 1:length(locs)
        RFO(locs(LL)) = RFO(locs(LL)-1)+2;
    end
    %        condition LHEEz
    figure(7)
    plot(LHS,'y')
    hold on
    plot([145 145],[7 0],'r')
%     plot([93 93], [5 0], 'r')
    [LHS_LHEEz_P, LHS_LHEEz_locs] = findpeaks(-LHEE_fs_c, 'MinPeakDistance',double(frate*0.6));%,'MinPeakHeight',-abs(LHEE_FS_sd_z));
    LHS(LHS_LHEEz_locs) = LHS(LHS_LHEEz_locs)+3;      
    plot(LHS, 'g')
%     [LHS_FA_P, LHS_FA_locs] = findpeaks(-abs(diffLFA_fs), 'MinPeakDistance',double(frate*0.6),'MinPeakHeight',-30);
%     LHS(LHS_FA_locs) = LHS(LHS_FA_locs)+1;
%     plot(LHS,'k')
%     LHS = FindLocalPeakAfterMax(LHS,SACR_LHEE_x_v,nframes,0.2,0.5,500);
%     plot(LHS, 'b')
%     LHS = FindLocalPeakAfterMax(LHS,LHEE_v_z, nframes, 0.2, 0.5, 500);
%     plot(LHS, 'r')
    figure(8)
    plot(RHS, 'b')
    [RHS_RHEEz_p, RHS_RHEEz_locs] = findpeaks(-RHEE_fs_c, 'MinPeakDistance',double(frate*0.6));%,'MinPeakHeight',-abs(RHEE_FS_sd_z));
    RHS(RHS_RHEEz_locs) = RHS(RHS_RHEEz_locs)+3;   
    hold on
    plot(RHS, 'r')
%     [RHS_FA_P, RHS_FA_locs] = findpeaks(-abs(diffRFA_fs), 'MinPeakDistance',double(frate*0.6),'MinPeakHeight',-30);
%     RHS(RHS_FA_locs) = LHS(LHS_FA_locs)+1;
%     RHS = FindLocalPeakAfterMax(RHS,SACR_RHEE_x_v,nframes,0.2,0.5,500);
%     RHS = FindLocalPeakAfterMax(RHS,RHEE_v_z, nframes, 0.2, 0.5, 500);
%     plot(RHS, 'b')


    %% Detect Peaks of checking signal
    [LHS_peaks, LHS_locs] = findpeaks(LHS, 'MinPeakDistance',double(frate*0.6),'MinPeakHeight',1.5); % nframes*0.1
    [RHS_peaks, RHS_locs] = findpeaks(RHS, 'MinPeakDistance',double(frate*0.6),'MinPeakHeight',1.5);
    [LFO_peaks, LFO_locs] = findpeaks(LFO, 'MinPeakDistance',double(frate*0.6),'MinPeakHeight',1.5);
    [RFO_peaks, RFO_locs] = findpeaks(RFO, 'MinPeakDistance',double(frate*0.6),'MinPeakHeight',1.5);
    
    %% Plot 
    if ~isempty(Check_EvFr(n).LFS)
        figure(1)
        plotEventDetect_param_fs(Check_EvFr(n).LFS, LHEE_fs_c, Ldiff_Angle_fs, SACR_LHEE_x_v, ff, LHS_locs)
        figure(3)
        plotEventDetect_param_fo(Check_EvFr(n).LFO, LHEE_fo_c, Ldiff_Angle_fo, LTOE_fo_c, SACR_LTOE_x, ff, locs)
        difflfs = abs(LHS_locs - (Check_EvFr(n).LFS - ff));
        lfs_err = [lfs_err, min(difflfs)];
        disp(['[Check] Left foot strike event error:', num2str(min(difflfs)), ' frames.'])
        difflfo = abs(LFO_locs - (Check_EvFr(n).LFO - ff));
        lfo_err = [lfo_err, min(difflfo)];
        disp(['[Check] Left foot off event error:', num2str(min(difflfo)), ' frames.'])
    else
        disp(['[Check] Left event error: No reference event.'])
    end
    
    if ~isempty(Check_EvFr(n).RFS)
        figure(2)
        plotEventDetect_param_fs(Check_EvFr(n).RFS, RHEE_fs_c, Rdiff_Angle_fs, SACR_RHEE_x_v, ff, RHS_locs)
        figure(4)
        plotEventDetect_param_fo(Check_EvFr(n).RFO, RHEE_fo_c, Rdiff_Angle_fo, RTOE_fo_c, SACR_LTOE_x, ff, locs)
        diffrfs = abs(RHS_locs - (Check_EvFr(n).RFS - ff));
        rfs_err = [rfs_err, min(diffrfs)];
        disp(['[Check] Right foot strike event error:', num2str(min(diffrfs)), ' frames.'])
        diffrfo = abs(RFO_locs - (Check_EvFr(n).RFO - ff));
        rfo_err = [rfo_err, min(diffrfo)];
        disp(['[Check] Right foot off event error:', num2str(min(diffrfo)), ' frames.'])
    else
        disp(['[Check] Right event error: No reference event.'])
    end       
    
    %% Check for erroneus events
    [LHS_locs, LFO_locs] = check_footevents(LHS_locs, LFO_locs, C3D);
    [RHS_locs, RFO_locs] = check_footevents(RHS_locs, RFO_locs, C3D);
    
    %% Clear events
    INFO = btkMetaDataInfo(format, values);
    btkClearEvents(acq);
    %% Append events
    for l_fs = 1:length(LHS_locs) 
        btkAppendEvent(acq, 'Foot Strike', (LHS_locs(l_fs)+ff)/frate, 'Left','','',1);
    end
    for r_fs = 1:length(RHS_locs) 
        btkAppendEvent(acq, 'Foot Strike', (RHS_locs(r_fs)+ff)/frate, 'Right','','',1);
    end
    for l_fo = 1:length(LFO_locs) 
        btkAppendEvent(acq, 'Foot Off', (LFO_locs(l_fo)+ff)/frate, 'Left','','',2);
    end
    for r_fo = 1:length(RFO_locs) 
        btkAppendEvent(acq, 'Foot Off', (RFO_locs(r_fo)+ff)/frate, 'Right','','',2);
    end
    MM=btkAppendMetaData(acq, 'EVENT', 'Method',INFO);
    btkWriteAcquisition(acq,[C3D_path, C3D_filename{n}]);
    btkCloseAcquisition(acq);
end

disp(['[Check] FINAL MEAN(SD) Left foot strike event error:', num2str(mean(lfs_err)), '(',num2str(std(lfs_err)) ') frames.'])
disp(['[Check] FINAL MEAN(SD) Left foot Off event error:', num2str(mean(lfo_err)), '(',num2str(std(lfo_err)) ') frames.'])
disp(['[Check] FINAL MEAN(SD) Right foot strike event error:', num2str(mean(rfs_err)),  '(',num2str(std(rfs_err)) ') frames.'])
disp(['[Check] FINAL MEAN(SD) Right foot Off event error:', num2str(mean(rfo_err)), '(',num2str(std(rfo_err)) ') frames.'])