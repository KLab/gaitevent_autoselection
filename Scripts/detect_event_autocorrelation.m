function event_fr = detect_event_autocorrelation(mcoord, ref_mcoord)

mfields = fieldnames(mcoord);
rfields = fieldnames(ref_mcoord);
event_fr = [];

for i = 1:size(mcoord,2)
    % calculate the mean of the marker coordinate for normalization
    mean_mcoord = mean(mcoord.(mfields(i)));
    detection = mcoord.(mfields) - mean_mcoord - ref_mcoord.(rfields(i));
    [peak, locs] = findpeaks(-detection(2:end), 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',max(-detection)-(max(-detection)-mean(-detection))*0.9);
    event_fr = [event_fr; locs];
end

final_event_fr = [];
locs = sort(event_fr);
for i = 1:length(locs)-1
    if  locs(i+1)-locs(i)<30
        final_event_fr = [final_event_fr, 1+mean(locs(i:i+1))];
    end
end

end