function Theta = CalculateAngle_FootFloor(heel, toe)
% calculate angle between floor and foot on the sagital plane
% heel: heel marker coordinates on the event frames
% toe: toe marker coordinates on the event frames

% Calculate foot angle at both FS and TOE for all valid cycles in FP
FP0 = [0,0];
FP1 = [1,0];
floor_v = FP1-FP0;
Foot_v = [];
Theta = []; 

if size(heel,1) == 0
    disp('No Valid left foot strike on platform detected on the overall session')
else
    for i = 1:size(heel,1)
        % create foot vector
        Foot_v = [Foot_v; toe(i,1) - heel(i,1), toe(i,3) - heel(i,3)];
        % correct if negative sense
        if toe(i,1)-heel(i,1) < 0 
            Foot_v(i,1)=-Foot_v(i,1);
        end
        % calculate angle        
        Theta = [Theta, real(acosd(dot(Foot_v(i,:),floor_v(:))/(norm(Foot_v(i,:))*norm(floor_v(:)))))];
    end
end
end