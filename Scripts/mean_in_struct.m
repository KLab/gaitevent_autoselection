function output_mean = mean_in_struct(struct, field1, field2)

output = []
for i = 1:length(struct)
    output = struct(i).(field1).(field2);
end

output_mean = mean(output)
end