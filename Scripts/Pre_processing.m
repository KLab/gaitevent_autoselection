% M.Fonseca May 2021 

%Pre-processing routine for c3d files
% Includes:
% 1....

% C3D_path_defaut='C:\Users\FONSECMI\OneDrive - unige.ch\Projects\VAR\Healthy data\Data\Healthy Gait'
% C3D_path_defaut = 'H:\Sofamehack\2018-Event detection\C3D Checked\CP_ANONYMOUS_GBNNN_FP_GMFCS_CUT'
C3D_path_defaut = 'H:\Sofamehack\2018-Event detection\ITW_GBNNN_FP_CUT_ALL_MARKERS_VALIDITY_CHECK'
[C3D_filename,C3D_path,FilterIndex]=uigetfile({'*.c3d'},'S?lectionner les ficihers C3D ',C3D_path_defaut,'MultiSelect','on');
% export_path = 'C:\Users\FONSECMI\OneDrive - unige.ch\Projects\VAR\Data_processing_fullPyCGM2\Scripts\';
export_path = 'C:\Users\FONSECMI\OneDrive - unige.ch\Projects\GEV\Data\Sofameak\Data CP'
for i = 1:length(C3D_filename)
    copyfile([C3D_path C3D_filename{i}],export_path); % copy files to computed_datapath
end

F.Marker=6;
F.ForcePlate=10;

for d = 1:length(C3D_filename)
    filename = C3D_filename{d};
    acq=btkReadAcquisition(strcat(C3D_path, filename));
    md = btkFindMetaData(acq, 'MANUFACTURER', 'SOFTWARE');
    MANUFACTURER=char(md.info.values);
    AlreadyProcessedBin=0;
    labelanlge=btkGetAngles(acq);
    labelpower=btkGetPowers(acq);
    labelmoment=btkGetMoments(acq);
    labelforce=btkGetForces(acq);
    label=[fieldnames(labelanlge);fieldnames(labelpower);fieldnames(labelmoment);fieldnames(labelforce)];
    AlreadyProcessedBin=RenameAlreadyProcessedLabels(MANUFACTURER,label,AlreadyProcessedBin,acq,strcat(C3D_path,filename));
            
    %% FILL GAP on trajectories - 20.11.2019 (ANNE)
    acq=btkReadAcquisition(strcat(C3D_path,filename));%'\',
    [MARKERS MARKERSINFO]=btkGetMarkers(acq);
    freq=btkGetPointFrequency(acq);
    list_markers=fieldnames(MARKERS);
    optimal_list_markers={'LFHD','RFHD','LBHD','RBHD','C7','T10','CLAV','STRN','RBAK',...
        'LSHO','LELB','LWRA','LWRB','LFIN','RSHO','RELB','RWRA','RWRB','RFIN',...
        'LASI','RASI','LPSI','RPSI','SACR',...
        'LTHI','LKNE','LTIB','LTTU','LANK','RTHI','RKNE','RTIB','RTTU','RANK',...
        'LTOE','LHEE','LMED','LMET','RTOE','RHEE','RMED','RMET','RTIAD','LTIAD',...
        'LFMH', 'RFMH','RTAD','LTAD','RTHAP','LTHAP'};
    list_markers_final=list_markers(find(ismember(list_markers,optimal_list_markers)));
    C3D = Get_C3D_BTK_Anonyme(C3D_path,filename,0);
    
%     %% Fill gaps
%     C3D = FIND_AND_FILL_GAP(C3D,list_markers_final);
    
%     %% Correct FP
%     C3D = Get_C3D_BTK_Anonyme(C3D_path,filename,0);
%     Correct_FP_C3D_MokkaForMickael(C3D.acq, filename);
%     
%     %% Gait Events 
%     C3D = Get_C3D_BTK_Anonyme(C3D_path,filename,0);
%     C3D = GaitCycleEvents(C3D,1); %Zeni

%     %% Filtring of markers - ANNE (04.12.19)
%     C3D = Get_C3D_BTK_Anonyme(C3D_path,filename,0);
%     mdf=btkGetMetaData(C3D.acq);
    
%     if ~isfield(mdf.children,'PROCESSING')
% %         filterMarkerTrajectoryPyCGM(C3D, F.Marker, 4,list_markers_final); % modified - filter all the signal
% %         disp([C3D.filename 'Filtering Trajectories - Butterwoth 4th Order - ' num2str(F.Marker)]);
% %         values={['Butterworth low-pass filter (cut-off: ' num2str(F.Marker) 'Hz, order: 4)']};
% %         format='Char';
% %         INFO = btkMetaDataInfo(format, values) ;
% %         btkAppendMetaData(C3D.acq, 'PROCESSING', 'Marker_filter_Processing',INFO);
% %         btkAppendMetaData(C3D.acq, 'MARKERS', 'Filter',INFO);
%         btkWriteAcquisition(C3D.acq,strcat(C3D.pathname,C3D.filename));
%     elseif ~isfield(mdf.children.PROCESSING.children,'Marker_filter_Processing')
% %         filterMarkerTrajectoryPyCGM(C3D, F.Marker, 4,list_markers_final); % modified - filter all the signal
% %         disp([C3D.filename 'Filtering Trajectories - Butterwoth 4th Order - ' num2str(F.Marker)]);
% %         values={['Butterworth low-pass filter (cut-off: ' num2str(F.Marker) 'Hz, order: 4)']};
% %         format='Char';
% %         INFO = btkMetaDataInfo(format, values) ;
% %         btkAppendMetaData(C3D.acq, 'PROCESSING', 'Marker_filter_Processing',INFO);
% %         btkAppendMetaData(C3D.acq, 'MARKERS', 'Filter',INFO);
%         btkWriteAcquisition(C3D.acq,strcat(C3D.pathname,C3D.filename));
%     end
%     
%     %% MANAGE MISSING MARKERS -  22.11.2019 (ANNE)
%     M.Model_Output=ReadModelOutputMissingMarkers(C3D.acq);
%     btkCloseAcquisition(C3D.acq);
%     Markers={'C7','CLAV','T10','STRN','LBHD','LFHD','RBHD','RFHD','LSHO',...
%         'LELB','LWRA','LWRB','LFIN','RSHO','RELB','RWRA','RWRB','RFIN',....
%         'LPSI','LASI','RPSI','RASI',...
%         'LTHI','LKNE','LTIB','LANK','LHEE','LTOE',...
%         'RTHI','RKNE','RTIB','RANK','RHEE','RTOE'};
%     for m=1:length(Markers)
%         M.Markers(m).name=Markers{m};
%     end
%     M.MinimalMarkersSet=Markers;
%     [MFinal, passe_missing_markers]=ManageMissingMarker(C3D,M);
%     
%     %% Check foot in the force platforms - 01.01.2019 (M.Fonseca)
%     Foot = Check_Foot_ForcePlate_V3(C3D_path,filename);  % Check if feet are correctly over the platforms
%     
end