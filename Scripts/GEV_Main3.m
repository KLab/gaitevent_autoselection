% Author:   M. Fonseca
%           Kinesiology Laboratory (K-LAB)
%           University of Geneva
%           https://www.unige.ch/medecine/kinesiology

% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/ (...)
% Reference  :   "Automatic gait event detection in pathologic gait using an auto-selection  approach" 
%                M. Fonseca, R. Dumas, S. Armand; Gait & Posture
% Date       :   June 2021
% -------------------------------------------------------------------------
% Description:   Routine to check whether the fusion of motion capture and medical imaging
%                can reduce the marker misplacement errors
% Process    :   1 - Calculate reference database: extract gait event frames by GRF + discrete points from marker trajectories at same frames
%                2 - Model implementation
%                3 - Detection of most accurate model
%                4 - Results visualization
%                
% Dependencies : https://github.com/Biomechanical-ToolKit/BTKCore
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

%% 1. Calculate reference database: extract gait event frames by GRF + discrete points from marker trajectories at same frames
% 1.1 Load list of (.c3d) files (works for batch processing)
% path to scripts
main_path = 'C:\Users\FONSECMI\OneDrive - unige.ch\Projects\GEV\Scripts';
% path to data
C3D_path_defaut='C:\Users\mcdf\OneDrive - HOPITAUX UNIVERSITAIRES DE GENEVE\Gitlab\Gait Events\Data\test\';
% path to data with events computed by DeepEvents (Lempereur et al. 2020)
c3d_path_DEV = 'C:\Users\FONSECMI\OneDrive - unige.ch\Projects\GEV\Data\Sofameak\Deepevents\all\';
% manual selection of data (.c3d)
[C3D_filename,C3D_path,FilterIndex]=uigetfile({'*.c3d'},'S?lectionner les ficihers C3D ',C3D_path_defaut,'MultiSelect','on');
% DeepEvents computed?
DE_check = 0;
% List the different sessions encountered
list_sessions = {};
for c =1:length(C3D_filename)
    sess = C3D_filename{c}(1:11);
    if ~any(strcmp(list_sessions, sess))
        list_sessions{end+1} = sess;
    end
end

% 1.3 Define inital treshold parameters
% threshold for GRF detection of FS and FO
threshold_FP_detection = 20;
% foot_wide_threshold for clean step detection, default 1.2 (increment of 20% of foot width)
foot_wide_threshold = 1.2;
% foot_front_threshol for clean step detection, default 1.1 (increment of 10% of foot length [heel-toe + 10%])
foot_front_threshold = 1.1;
tic
Accuracy_tab = [];
for s = 1:length(list_sessions) 
    Sess_files = {};
    session_files = strfind(C3D_filename, list_sessions{s});
    for lsess =1:length(C3D_filename)
        if session_files{lsess} == 1
            Sess_files{end+1} = C3D_filename{lsess};
        end
    end
    % Get anthropometric data
    acq = btkReadAcquisition(strcat(C3D_path, Sess_files{1}));
    md = btkGetMetaData(acq);
    Anthropo(s).age = str2num(cell2mat(md.children.SUBJECTS.children.AGE.info.values));
    Anthropo(s).gender = cell2mat(md.children.SUBJECTS.children.SEX.info.values);
    Anthropo(s).Diagnostic = cell2mat(md.children.SUBJECTS.children.DIAGNOSTIC.info.values);
    if isfield(md.children.SUBJECTS.children, 'GMFCS')
        Anthropo(s).GMFCS = str2num(cell2mat(md.children.SUBJECTS.children.GMFCS.info.values));
    end
    Anthropo(s).Side = cell2mat(md.children.SUBJECTS.children.AFFECTED_SIDE.info.values);
    if isfield(md.children, 'PROCESSING')
        if isfield(md.children.PROCESSING.children, 'Bodymass')
            Anthropo(s).Bodymass = md.children.PROCESSING.children.Bodymass.info.values;
            Anthropo(s).Height = md.children.PROCESSING.children.Height.info.values;
        end      
    end

    % 1.2 Initiate foot coordinate variables to extract
    Check_EvFr = struct('LFS',[], 'LFO',[], 'RFS',[],'RFO',[]);
    LEvFoot = [];
    REvFoot = [];
    fromat = 'Char';
    
    % 1.4 Check valid event in force platform
    n = 0;
    for file=1:length(Sess_files)
        C3D = Get_C3D_BTK(C3D_path,Sess_files{file},0);
        btkClearEvents(C3D.acq);% clear events
        btkWriteAcquisition(C3D.acq,[C3D.pathname, Sess_files{file}]);
    
        % create SACR file if missing by midpoint between post iliac spines
        C3D = Get_C3D_BTK(C3D_path,Sess_files{file},0);   
        if ~isfield(C3D.data, 'SACR')
            C3D.data.SACR = (C3D.data.LPSI' + C3D.data.RPSI').'/2;
        end
        disp(Sess_files{file})
        n = n+1;
        
        [LEvFoot, REvFoot, Check_EvFr] =  Check_Foot_ForcePlate_3(C3D, threshold_FP_detection, LEvFoot, REvFoot, foot_front_threshold, foot_wide_threshold, Check_EvFr, n);
        n = length(Check_EvFr);      
    end
    
    %Display
    disp('-----------------------------------------------------------------------')
    disp('Calibration events finalized')
    if ~isempty(LEvFoot)
        disp(['Left side - Number of valid events detected: ', num2str(length(LEvFoot(all(~cellfun(@isempty,struct2cell(LEvFoot))))))])
        LEvFoot = LEvFoot(all(~cellfun(@isempty,struct2cell(LEvFoot))));
    else
        LEvFoot = [];
        disp(['No reference gait event found on the left side for the patient ',Sess_files{file}])
    end
    if ~isempty(REvFoot)
        disp(['Right side - Number of valid events detected: ', num2str(length(REvFoot(all(~cellfun(@isempty,struct2cell(REvFoot))))))]) 
        REvFoot = REvFoot(all(~cellfun(@isempty,struct2cell(REvFoot))));
    else
        REvFoot = [];
        disp(['No reference gait event found on the right side for the patient ',Sess_files{file}])
    end    
    disp('                                                                       ')
    disp('-----------------------------------------------------------------------')
    
    % initialize accuracy variables
    Accuracy.FS.acc_AC1 = []; Accuracy.FS.acc_AC2 = []; Accuracy.FS.acc_AC3 = [];
    Accuracy.FS.acc_AC4 = []; Accuracy.FS.acc_ZN = []; Accuracy.FS.acc_GO = [];
    Accuracy.FO.acc_AC1 = []; Accuracy.FO.acc_ZN = []; Accuracy.FO.acc_GO = [];
    Accuracy.FO.acc_AC2 = []; Accuracy.FO.acc_AC3 = [];Accuracy.FS.acc_DEV = [];
    Accuracy.FO.acc_DEV = []; Accuracy.FS.acc_DY = []; Accuracy.FO.acc_DY = [];
    %% 2. Model implementation
    % LEFT SIDE
    if ~isempty(LEvFoot)
        for nL = 1:length(LEvFoot)
            LF_temp = LEvFoot;
            file = LF_temp(nL).Filename;
            % remove the entries on the reference struct belong to file
            toRemove = strcmp({LF_temp.Filename}, file);
            LF_temp(toRemove) = [];

            % calculate mean of the parameters from reference database
            LREF = mean_refparameters(LF_temp);
            % Read c3d data
            C3D = Get_C3D_BTK(C3D_path,file,0);
            markers = C3D.data;
            fp  = btkGetForcePlatforms(C3D.acq);
            frate = C3D.fRate.Point;
            metadata = btkGetMetaData(C3D.acq);
            ff = C3D.StartFrame;
            lf = C3D.EndFrame;
            Ev = C3D.EventFrame;

            if isfield(Ev, 'Left_Lfoot_Strike')
                EvL(nL).FS.Ref = Ev.Left_Lfoot_Strike;
                EvL(nL).FO.Ref = Ev.Left_Lfoot_Off;
                Left_foot_strike = 'Left_Lfoot_Strike';
                Left_foot_off = 'Left_Lfoot_Off';
            elseif isfield(Ev, 'Left_Foot_Strike')
                EvL(nL).FS.Ref = Ev.Left_Foot_Strike;
                EvL(nL).FO.Ref = Ev.Left_Foot_Off;
                Left_foot_strike = 'Left_Foot_Strike';
                Left_foot_off = 'Left_Foot_Off';
            else
                disp('[ERROR] Foot event name not identified.')
            end
            nframes = length(markers.LHEE(:,1));

            % Add SACR marker in case of missing, the midpoint between the 2 PSI
            if ~isfield(C3D.data, 'SACR')
                % filter marker trajectories
                filterMarkerTrajectoryPyCGM(C3D, 4, 6,{'LHEE','LTOE','RHEE','RTOE','LPSI','RPSI','LKNE', 'RKNE'});
                markers.SACR = (markers.LPSI' + markers.RPSI').'/2;
            else
                % filter marker trajectories
                filterMarkerTrajectoryPyCGM(C3D, 4, 6,{'LHEE','LTOE','RHEE','RTOE','SACR','LKNE', 'RKNE'});
            end
            %     Get the sense of walking
            if markers.SACR(1,1)>markers.SACR(end,1)
                sense = -1; % right to left
            else
                sense = 1; % left to right
            end

            % Foot Strike by autocorrelation Left side
            norm_LHEEz = Normalize_vectors(markers.LHEE(:,3));
            norm_SACRz = Normalize_vectors(markers.SACR(:,3));
            norm_LTOEz = Normalize_vectors(markers.LTOE(:,3));
            norm_Hipx_fs = Normalize_vectors(sense*(markers.LASI(:,1)-markers.RASI(:,1)));
            norm_Foot_far_fs = Normalize_vectors(sense*(markers.LHEE(:,1) - markers.SACR(:,1)));
            norm_Foot_far_fo = Normalize_vectors(sense*(markers.LTOE(:,1) - markers.SACR(:,1)));
            norm_footangle_fo = Normalize_vectors(markers.LHEE(:,3) - markers.LTOE(:,3));
            foot = [(markers.LTOE(:,1)-markers.LHEE(:,1)) (markers.LTOE(:,3)-markers.LHEE(:,3))];
            floor = [(markers.LTOE(:,1)-markers.LHEE(:,1)) zeros(length(markers.LTOE),1)];
            VdotF = (foot(:,1).*floor(:,1) + foot(:,2).*floor(:,2));
            norm_footang = Normalize_vectors(acosd(VdotF/(norm(floor)*norm(foot))));
            
            if mean(cat(3, LF_temp(:).Foot_far_FS)) < 0.9
                disp('[WARNING] Foot far reference to low')
            end
            if mean(cat(3,LF_temp(:).LHEEz_FS)) > 0.1
                disp('[WARNING]LHEEz reference to high')
            end
            % Calculate difference to the reference values
            diff_LHEEz_FS = abs(norm_LHEEz - mean(cat(3,LF_temp(:).LHEEz_FS)));
            diff_Foot_far_fs = abs(norm_Foot_far_fs - mean(cat(3, LF_temp(:).Foot_far_FS)));
            diff_Hipx_fs = abs(norm_Hipx_fs - mean(cat(3, LF_temp(:).LHipx_fs)));
            diff_LFA_fs = abs(norm_footang - mean(cat(3,LF_temp(:).LFA_fs)));
            
            % AC1
            predictor_lfs_1 = diff_Foot_far_fs +  diff_LHEEz_FS ;
            % AC2
            predictor_lfs_2 = predictor_lfs_1 + diff_LFA_fs;
            % AC3
            predictor_lfs_3 = diff_Foot_far_fs + diff_Hipx_fs;
            % AC4
            predictor_lfs_4 = diff_Foot_far_fs + diff_Hipx_fs + diff_LHEEz_FS;
            
            % Detect event frames for each method by min peak detection
            [LFS_pks, LFS_locs_AC1] = findpeaks(-predictor_lfs_1, 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',-1);
            [LFS_pks, LFS_locs_AC2] = findpeaks(-predictor_lfs_2, 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',-1);
            [LFS_pks, LFS_locs_AC3] = findpeaks(-predictor_lfs_3, 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',-1);
            [LFS_pks, LFS_locs_AC4] = findpeaks(-predictor_lfs_4, 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',-1);
            
            % Store event frames
            EvL(nL).FS.AC1 = LFS_locs_AC1;
            EvL(nL).FS.AC2 = LFS_locs_AC2;
            EvL(nL).FS.AC3 = LFS_locs_AC3;
            EvL(nL).FS.AC4 = LFS_locs_AC4;
            
            %% Calculate gait events with Zeni model (keep only left side)
            C3D = GaitCycleEvents(C3D,1);
            EvL(nL).FS.ZN = C3D.EventFrame.Left_Foot_Strike_D;
            EvL(nL).FO.ZN = C3D.EventFrame.Left_Foot_Off_D;

            %% calculate gait events with Ghaussani model (keep only left side)
            [FS_L,~, FO_L, ~] = Ghoussayni(C3D);
            EvL(nL).FS.GO = FS_L;
            EvL(nL).FO.GO = FO_L;
            
            %% Kinematic algorithm_Desailly (keep only left side)
            if sense == 1
                walkdir = 1;
            elseif sense == -1
                walkdir = 2;
            end
            gaitAxis=1;
            sgn = sign(markers.SACR(end, walkdir) - markers.SACR(1, walkdir));
            walkdir = walkdir * sgn;
            [Markers_Corrected]=f_rotCoordinateSystem(markers, walkdir, 1);
            [B,A] = butter(4,(7/(frate/2)));
            filttoemarker_d = filtfilt(B, A, Markers_Corrected.LTOE);
            filtheelmarker_d = filtfilt(B, A, Markers_Corrected.LHEE);
            fhm2 = filttoemarker_d(1:end,gaitAxis);
            fhm=filtheelmarker_d(1:end,gaitAxis);
            [z,p,k] = butter(4,0.5/(frate/2),'high');
            [sos,g] = zp2sos(z,p,k);
            toe_high  = filtfilt(sos,g,fhm2);
            heel_high=filtfilt(sos,g,fhm);

            [~,index_d_TO]=findpeaks(-toe_high, 'MinPeakDistance',double(frate*0.8));
            [~,index_d_FS]=findpeaks(heel_high, 'MinPeakDistance',double(frate*0.8));
            EvL(nL).FS.DY = index_d_FS;
            EvL(nL).FO.DY = index_d_TO;
            
%             %% O'Connor
%             LMidfoot = sqrt((markers.LHEE - markers.LTOE ) .^ 2)
%             [~, LMidfoot_vel] = acceleration_vector(LMidfoot(:,3));
%             [~, LFS_locs_OCO] = findpeaks(-LMidfoot_vel(:,1), 'MinPeakDistance',double(frate*0.8));
%             [~, LFO_locs_OCO] = findpeaks(LMidfoot_vel(:,1), 'MinPeakDistance',double(frate*0.8));
%             EvL(nL).FS.CO = LFS_locs_OCO;
%             EvL(nL).FO.CO = LFO_locs_OCO;
%             
%             %% Hrejac (2000)
%             [LHEEz_acc, ~] = acceleration_vector(markers.LHEE(:,3));
%             [~, LFS_locs_HR] = findpeaks(LHEEz_acc(:,1), 'MinPeakDistance',double(frate*0.8));
%             [LTOEz_acc, ~] = acceleration_vector(markers.LTOE(:,3)-mean(markers.LTOE(:,3)));
%             [~, LFO_locs_HR] = findpeaks(LTOEz_acc(:,1), 'MinPeakDistance',double(frate*0.8));
%             
%             %% Hsue (2007)
%             [LHEEx_acc, ~] = acceleration_vector(markers.LHEE(:,1));
%             [~, LFS_locs_HR] = findpeaks(LHEEx_acc(:,1), 'MinPeakDistance',double(frate*0.8));
%             
            
            %% Read Deepevent event already calculated (left side)        
            if DE_check == 1
                dev_acq = btkReadAcquisition(strcat(c3d_path_DEV,file));
                Dev = btkGetEvents(dev_acq);
                fr = btkGetPointFrequency(dev_acq);
                ff = btkGetFirstFrame(dev_acq);
                if isfield(Dev, 'Left_Foot_Off')
                    EvL(nL).FO.DEV = (Dev.Left_Foot_Off*fr)-ff+1;
                end
                if isfield(Dev, 'Left_Foot_Strike')
                    EvL(nL).FS.DEV = (Dev.Left_Foot_Strike*fr)-ff+1;
                end
            else
                EvL(nL).FO.DEV = 0;
                EvL(nL).FS.DEV = 0;
            end
            %% Foot off
            diff_LTOEz_FO = abs(norm_LTOEz - mean(cat(3,LF_temp(:).LTOEz_FO)));
            diff_Foot_far_FO = abs(norm_Foot_far_fo - mean(cat(3, LF_temp(:).Foot_far_FO)));
            diff_LFA_fo = abs(norm_footang - mean(cat(3, LF_temp(:).LFA_FO)))
            
            % AC5
            predictor_lfo = diff_LTOEz_FO +  diff_Foot_far_FO;
            % AC6
            predictor_lfo2 = predictor_lfo + diff_LFA_fo;
            
            [~, LFO_locs_AC5] = findpeaks(-predictor_lfo, 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',-0.4);
            [~, LFO_locs_AC6] = findpeaks(-predictor_lfo2, 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',-0.4);

            EvL(nL).FO.AC1 = LFO_locs_AC5+3;
            EvL(nL).FO.AC2 = LFO_locs_AC6+3;
            
            %% Calculate difference between predictions
            % Diff REF-AC
            % Foot Strike
            for e = 1:length(EvL(nL).FS.Ref)
                if ~isempty(EvL(nL).FS.AC1)
                    [~, i] = min(abs(EvL(nL).FS.AC1 - (EvL(nL).FS.Ref(e))));
                    if min(abs(EvL(nL).FS.AC1 - (EvL(nL).FS.Ref(e))))<18
                        Accuracy.FS.acc_AC1(end+1) = (EvL(nL).FS.AC1(i) - (EvL(nL).FS.Ref(e))+1);
                    end
                end
                if ~isempty(EvL(nL).FS.AC2)
                    [~, i] = min(abs(EvL(nL).FS.AC2 - (EvL(nL).FS.Ref(e))));
                    Accuracy.FS.acc_AC2(end+1) = (EvL(nL).FS.AC2(i) - (EvL(nL).FS.Ref(e)));
                end
                if ~isempty(EvL(nL).FS.AC3)
                    [~, i] = min(abs(EvL(nL).FS.AC3 - (EvL(nL).FS.Ref(e))));
                    Accuracy.FS.acc_AC3(end+1) = (EvL(nL).FS.AC3(i) - (EvL(nL).FS.Ref(e)));
                end
                if ~isempty(EvL(nL).FS.AC4)
                    [~, i] = min(abs(EvL(nL).FS.AC4 - (EvL(nL).FS.Ref(e))));
                    if min(abs(EvL(nL).FS.AC1 - (EvL(nL).FS.Ref(e))))<22
                        Accuracy.FS.acc_AC4(end+1) = (EvL(nL).FS.AC4(i) - (EvL(nL).FS.Ref(e)));
                    end
                end
                if ~isempty(EvL(nL).FS.ZN)
                    [~, i] = min(abs(EvL(nL).FS.ZN - (EvL(nL).FS.Ref(e))));
                    Accuracy.FS.acc_ZN(end+1) = (EvL(nL).FS.ZN(i) - (EvL(nL).FS.Ref(e)));
                end
                if ~isempty(EvL(nL).FS.GO)
                    [~, i] = min(abs(EvL(nL).FS.GO - (EvL(nL).FS.Ref(e))));
                    Accuracy.FS.acc_GO(end+1) = (EvL(nL).FS.GO(i) - (EvL(nL).FS.Ref(e)));
                end
                if ~isempty(EvL(nL).FS.DY)
                    [~, i] = min(abs(EvL(nL).FS.DY - (EvL(nL).FS.Ref(e))));
                    Accuracy.FS.acc_DY(end+1) = (EvL(nL).FS.DY(i) - (EvL(nL).FS.Ref(e)));
                end
                if ~isempty(EvL(nL).FS.DEV)
                    [~, i] = min(abs(EvL(nL).FS.DEV - (EvL(nL).FS.Ref(e))));
                    Accuracy.FS.acc_DEV(end+1) = (EvL(nL).FS.DEV(i) - (EvL(nL).FS.Ref(e)));
                end
            end
            % Foot off
            for e = 1:length(EvL(nL).FO.Ref)
                if ~isempty(EvL(nL).FO.AC1)
                    [~, i] = min(abs(EvL(nL).FO.AC1 - (EvL(nL).FO.Ref(e))));
                    Accuracy.FO.acc_AC1(end+1) = (EvL(nL).FO.AC1(i) - (EvL(nL).FO.Ref(e)));
                end
                if ~isempty(EvL(nL).FO.AC2)
                    [~, i] = min(abs(EvL(nL).FO.AC2 - (EvL(nL).FO.Ref(e))));
                    Accuracy.FO.acc_AC2(end+1) = (EvL(nL).FO.AC2(i) - (EvL(nL).FO.Ref(e)));
                end
                if ~isempty(EvL(nL).FO.ZN)
                    [~, i] = min(abs(EvL(nL).FO.ZN - (EvL(nL).FO.Ref(e))));
                    Accuracy.FO.acc_ZN(end+1) = (EvL(nL).FO.ZN(i) - (EvL(nL).FO.Ref(e)));
                end
                if ~isempty(EvL(nL).FO.GO)
                    [~, i] = min(abs(EvL(nL).FO.GO - (EvL(nL).FO.Ref(e))));
                    Accuracy.FO.acc_GO(end+1) = (EvL(nL).FO.GO(i) - (EvL(nL).FO.Ref(e)));
                end
                if ~isempty(EvL(nL).FO.DY)
                    [~, i] = min(abs(EvL(nL).FO.DY - (EvL(nL).FO.Ref(e))));
                    Accuracy.FO.acc_DY(end+1) = (EvL(nL).FO.DY(i) - (EvL(nL).FO.Ref(e)));
                end
                if ~isempty(EvL(nL).FO.DEV)
                    [~, i] = min(abs(EvL(nL).FO.DEV - (EvL(nL).FO.Ref(e))));
                    Accuracy.FO.acc_DEV(end+1) = (EvL(nL).FO.DEV(i) - (EvL(nL).FO.Ref(e)));
                end
            end
        end
    end
    
    if ~isempty(REvFoot)
        %% RIGHT SIDE
        for nR = 1:length(REvFoot)
            RF_temp = REvFoot;
            file = RF_temp(nR).Filename;
            FS = REvFoot(nR).RFS;
            FO = REvFoot(nR).RFO;
            % remove the entries on the reference struct belong to file
            toRemove = strcmp({RF_temp.Filename}, file);
            RF_temp(toRemove) = [];

            % calculate mean of the parameters from reference database
            RREF = mean_refparameters(RF_temp);
            % Read c3d data using BTK
            C3D = Get_C3D_BTK(C3D_path,file,0);
            markers = C3D.data;
            fp  = btkGetForcePlatforms(C3D.acq);
            frate = C3D.fRate.Point;
            metadata = btkGetMetaData(C3D.acq);
            ff = C3D.StartFrame;
            lf = C3D.EndFrame;
            if isfield(C3D, 'EventFrame')
                Ev = C3D.EventFrame;
                if isfield(Ev, 'Right_Rfoot_Strike')
                    EvR(nR).FS.Ref = Ev.Right_Rfoot_Strike;
                    EvR(nR).FO.Ref = Ev.Right_Rfoot_Off;
                    Right_foot_strike = 'Right_Rfoot_Strike';
                    Right_foot_off = 'Right_Rfoot_Off';
                elseif isfield(Ev, 'Right_Foot_Strike')
                    EvR(nR).FS.Ref = Ev.Right_Foot_Strike;
                    EvR(nR).FO.Ref = Ev.Right_Foot_Off;
                    Right_foot_strike = 'Right_Foot_Strike';
                    Right_foot_off = 'Right_Foot_Off';
                else
                    disp('[ERROR] Foot event name not identified.')
                end
            else
                Right_foot_strike = 'Right_Foot_Strike';
                Right_foot_off = 'Right_Foot_Off';
                Ev.Right_foot_strike = FS-ff;
                Ev.Right_foot_off = FO-ff;
                EvR(nR).FS.Ref = FS-ff;
                EvR(nR).FO.Ref = FO-ff;
            end

            nframes = length(markers.RHEE(:,1));

            % Add SACR marker in case of missing, the midpoint between the 2 PSI
            if ~isfield(C3D.data, 'SACR')
                % % filter marker trajectories
                filterMarkerTrajectoryPyCGM(C3D, 4, 6,{'LHEE','LTOE','RHEE','RTOE','LPSI','RPSI','LKNE', 'RKNE'});
                markers.SACR = (markers.LPSI' + markers.RPSI').'/2;
            else
                % % filter marker trajectories
                filterMarkerTrajectoryPyCGM(C3D, 4, 6,{'LHEE','LTOE','RHEE','RTOE','SACR','LKNE', 'RKNE'});
            end
            %     Get the sense of walking
            if markers.SACR(1,1)>markers.SACR(end,1)
                sense = -1; % right to left
            else
                sense = 1; % left to right
            end

            % Foot Strike by autocorrelation Right side
            norm_RHEEz = Normalize_vectors(markers.RHEE(:,3));
            norm_SACRz = Normalize_vectors(markers.SACR(:,3));
            norm_RTOEz = Normalize_vectors(markers.RTOE(:,3));
            norm_Hipx_fs = Normalize_vectors(sense*(markers.RASI(:,1)-markers.LASI(:,1)));
            norm_Foot_far_fs = Normalize_vectors(sense*(markers.RHEE(:,1) - markers.SACR(:,1)));
            norm_Foot_far_fo = Normalize_vectors(sense*(markers.RTOE(:,1) - markers.SACR(:,1)));
            norm_footangle_fo = Normalize_vectors(markers.RHEE(:,3) - markers.RTOE(:,3));
            foot = [(markers.RTOE(:,1)-markers.RHEE(:,1)) (markers.RTOE(:,3)-markers.RHEE(:,3))];
            floor = [(markers.RTOE(:,1)-markers.RHEE(:,1)) zeros(length(markers.RTOE),1)];
            VdotF = (foot(:,1).*floor(:,1) + foot(:,2).*floor(:,2));
            norm_footang = Normalize_vectors(acosd(VdotF/(norm(floor)*norm(foot))));
            
            if mean(cat(3, RF_temp(:).Foot_far_FS)) < 0.9
                disp('[WARNING] Foot far reference to low')
            end
            if mean(cat(3,RF_temp(:).RHEEz_FS)) > 0.1
                disp('[WARNING]RHEEz reference to high')
            end
            
            % Calculate difference to the reference values
            diff_RHEEz_FS = abs(norm_RHEEz - mean(cat(3,RF_temp(:).RHEEz_FS)));    
            diff_Foot_far_fs = abs(norm_Foot_far_fs - mean(cat(3, RF_temp(:).Foot_far_FS)));          
            diff_Hipx_fs = abs(norm_Hipx_fs - mean(cat(3, RF_temp(:).RHipx_fs)));     
            diff_RFA_fs = abs(norm_footang -mean(cat(3,RF_temp(:).RFA_fs)));
            
            % AC1
            predictor_rfs_1 = diff_Foot_far_fs +  diff_RHEEz_FS ;
            % AC2
            predictor_rfs_2 = predictor_rfs_1 + diff_RFA_fs;
            % AC3
            predictor_rfs_3 = diff_Foot_far_fs + diff_Hipx_fs;
            % AC4
            predictor_rfs_4 = diff_Foot_far_fs + diff_Hipx_fs + diff_RHEEz_FS;

            % Detect event frames for each method by min peak detection
            [RFS_pks, RFS_locs_AC1] = findpeaks(-predictor_rfs_1, 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',-0.4);
            [RFS_pks, RFS_locs_AC2] = findpeaks(-predictor_rfs_2, 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',-0.4);
            [RFS_pks, RFS_locs_AC3] = findpeaks(-predictor_rfs_3, 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',-0.4);
            [RFS_pks, RFS_locs_AC4] = findpeaks(-predictor_rfs_4, 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',-0.4);
            
            % Store event detected frames
            EvR(nR).FS.AC1 = RFS_locs_AC1;
            EvR(nR).FS.AC2 = RFS_locs_AC2;
            EvR(nR).FS.AC3 = RFS_locs_AC3;
            EvR(nR).FS.AC4 = RFS_locs_AC4;

            %Remove FP events
            C3D.EventFrame = [];
            
            %% Calculate gait events with Zeni model (keep only right side)
            C3D = GaitCycleEvents(C3D,1);
            EvR(nR).FS.ZN = C3D.EventFrame.Right_Foot_Strike_D;
            EvR(nR).FO.ZN = C3D.EventFrame.Right_Foot_Off_D;
            %% calculate gait events with Ghaussani model (keep only right side)
            [~,FS_R, ~, FO_R] = Ghoussayni(C3D);
            EvR(nR).FS.GO = FS_R;
            EvR(nR).FO.GO = FO_R;
            
            %%  Kinematic algorithm_Desailly (keep only right side)
            if sense == 1
                walkdir = 1;
            elseif sense == -1
                walkdir = 2;
            end
            gaitAxis=1;
            sgn = sign(markers.SACR(end, walkdir) - markers.SACR(1, walkdir));
            walkdir = walkdir * sgn;
            [Markers_Corrected]=f_rotCoordinateSystem(markers, walkdir, 1);
            [B,A] = butter(4,(7/(frate/2)));
            filttoemarker_d = filtfilt(B, A, Markers_Corrected.RTOE);
            filtheelmarker_d = filtfilt(B, A, Markers_Corrected.RHEE);
            fhm2 = filttoemarker_d(1:end,gaitAxis);
            fhm=filtheelmarker_d(1:end,gaitAxis);
            [z,p,k] = butter(4,0.5/(frate/2),'high');
            [sos,g] = zp2sos(z,p,k);
            toe_high  = filtfilt(sos,g,fhm2);
            heel_high=filtfilt(sos,g,fhm);

            [location_d_TO,index_d_TO]=findpeaks(-toe_high);
            [location_d_FS,index_d_FS]=findpeaks(heel_high);
            EvR(nR).FS.DY = index_d_FS;
            EvR(nR).FO.DY = index_d_TO;
            
            % deepevent right side
            if DE_check == 1
                dev_acq = btkReadAcquisition(strcat(c3d_path_DEV,file));
                Dev = btkGetEvents(dev_acq);
                fr = btkGetPointFrequency(dev_acq);
                ff = btkGetFirstFrame(dev_acq);
                if isfield(Dev, 'Right_Foot_Off')
                    EvR(nR).FO.DEV = (Dev.Right_Foot_Off*fr)-ff+1;
                end
                if isfield(Dev, 'Right_Foot_Strike')
                    EvR(nR).FS.DEV = (Dev.Right_Foot_Strike*fr)-ff+1;
                end
            else
                EvR(nR).FO.DEV = 0;
                EvR(nR).FS.DEV = 0;
            end
                
            
            %% Foot off
            diff_RTOEz_FO = abs(norm_RTOEz - mean(cat(3,RF_temp(:).RTOEz_FO)));
            diff_Foot_far_FO = abs(norm_Foot_far_fo - mean(cat(3, RF_temp(:).Foot_far_FO)));
            diff_footangle_FO = abs(norm_footangle_fo - mean(cat(3, RF_temp(:).footangle_FO)));
            diff_RFA_fo = abs(norm_footang - mean(cat(3,RF_temp(:).RFA_FO)));
            
            predictor_rfo = diff_RTOEz_FO +  diff_Foot_far_FO;
            predictor_rfo2 = predictor_rfo  + diff_RFA_fo;

            % event detection by min peaks
            [RFO_pks, RFO_locs_AC] = findpeaks(-predictor_rfo, 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',-0.4);
            [RFO_pks, RFO_locs_AC2] = findpeaks(-predictor_rfo2, 'MinPeakDistance',double(frate*0.6), 'MinPeakHeight',-0.4);
            
            % store events
            EvR(nR).FO.AC1 = RFO_locs_AC+3;
            EvR(nR).FO.AC2 = RFO_locs_AC2+3;

            %% Calculate difference between predictions
            % Diff REF-AC
            for e = 1:length(EvR(nR).FS.Ref)
                if ~isempty(EvR(nR).FS.AC1)
                    [~, i] = min(abs(EvR(nR).FS.AC1 - (EvR(nR).FS.Ref(e))));
                    if min(abs(EvR(nR).FS.AC1 - (EvR(nR).FS.Ref(e))))<18
                        Accuracy.FS.acc_AC1(end+1) = (EvR(nR).FS.AC1(i) - (EvR(nR).FS.Ref(e))+1);
                    end
                end
                if ~isempty(EvR(nR).FS.AC2)
                    [~, i] = min(abs(EvR(nR).FS.AC2 - (EvR(nR).FS.Ref(e))));
                    Accuracy.FS.acc_AC2(end+1) = (EvR(nR).FS.AC2(i) - (EvR(nR).FS.Ref(e)));
                end
                if ~isempty(EvR(nR).FS.AC3)
                    [~, i] = min(abs(EvR(nR).FS.AC3 - (EvR(nR).FS.Ref(e))));
                    Accuracy.FS.acc_AC3(end+1) = (EvR(nR).FS.AC3(i) - (EvR(nR).FS.Ref(e)));
                end
                if ~isempty(EvR(nR).FS.AC4)
                    [~, i] = min(abs(EvR(nR).FS.AC4 - (EvR(nR).FS.Ref(e))));
                    if min(abs(EvR(nR).FS.AC4 - (EvR(nR).FS.Ref(e))))<23
                        Accuracy.FS.acc_AC4(end+1) = (EvR(nR).FS.AC4(i) - (EvR(nR).FS.Ref(e)));
                    end
                end
                if ~isempty(EvR(nR).FS.ZN)
                    [~, i] = min(abs(EvR(nR).FS.ZN - (EvR(nR).FS.Ref(e))));
                    Accuracy.FS.acc_ZN(end+1) = (EvR(nR).FS.ZN(i) - (EvR(nR).FS.Ref(e)));
                end
                if ~isempty(EvR(nR).FS.GO)
                    [~, i] = min(abs(EvR(nR).FS.GO - (EvR(nR).FS.Ref(e))));
                    Accuracy.FS.acc_GO(end+1) = (EvR(nR).FS.GO(i) - (EvR(nR).FS.Ref(e)));
                end
                if ~isempty(EvR(nR).FS.DY)
                    [~, i] = min(abs(EvR(nR).FS.DY - (EvR(nR).FS.Ref(e))));
                    Accuracy.FS.acc_DY(end+1) = (EvR(nR).FS.DY(i) - (EvR(nR).FS.Ref(e)));
                end
                if ~isempty(EvR(nR).FS.DEV)
                    [~, i] = min(abs(EvR(nR).FS.DEV - (EvR(nR).FS.Ref(e))));
                    Accuracy.FS.acc_DEV(end+1) = (EvR(nR).FS.DEV(i) - (EvR(nR).FS.Ref(e)));
                end
                              
                for e = 1:length(EvR(nR).FO.Ref)
                    if ~isempty(EvR(nR).FO.AC1)
                        [~, i] = min(abs(EvR(nR).FO.AC1 - (EvR(nR).FO.Ref(e))));
                        Accuracy.FO.acc_AC1(end+1) = (EvR(nR).FO.AC1(i) - (EvR(nR).FO.Ref(e)));
                    end
                    if ~isempty(EvR(nR).FO.AC2)
                        [~, i] = min(abs(EvR(nR).FO.AC2 - (EvR(nR).FO.Ref(e))));
                        Accuracy.FO.acc_AC2(end+1) = (EvR(nR).FO.AC2(i) - (EvR(nR).FO.Ref(e)));
                    end
                    if ~isempty(EvR(nR).FO.ZN)
                        [~, i] = min(abs(EvR(nR).FO.ZN - (EvR(nR).FO.Ref(e))));
                        Accuracy.FO.acc_ZN(end+1) = (EvR(nR).FO.ZN(i) - (EvR(nR).FO.Ref(e)));
                    end
                    if ~isempty(EvR(nR).FO.GO)
                        [~, i] = min(abs(EvR(nR).FO.GO - (EvR(nR).FO.Ref(e))));
                        Accuracy.FO.acc_GO(end+1) = (EvR(nR).FO.GO(i) - (EvR(nR).FO.Ref(e)));
                    end
                    if ~isempty(EvR(nR).FO.DY)
                        [~, i] = min(abs(EvR(nR).FO.DY - (EvR(nR).FO.Ref(e))));
                        Accuracy.FO.acc_DY(end+1) = (EvR(nR).FO.DY(i) - (EvR(nR).FO.Ref(e)));
                    end
                    if ~isempty(EvR(nR).FO.DEV)
                        [~, i] = min(abs(EvR(nR).FO.DEV - (EvR(nR).FO.Ref(e))));
                        Accuracy.FO.acc_DEV(end+1) = (EvR(nR).FO.DEV(i) - (EvR(nR).FO.Ref(e)));
                    end
                end
            end
        end
    end
    
    %% 3. Detection of most accurate model
    % 3.1 calculate mean accuracy for each session (in ms)
    Err_AC1_FS = mean(Accuracy.FS.acc_AC1)*10;
    Err_AC2_FS = mean(Accuracy.FS.acc_AC2)*10;
    Err_AC3_FS = mean(Accuracy.FS.acc_AC3)*10;
    Err_AC4_FS = mean(Accuracy.FS.acc_AC4)*10;
    Err_ZN_FS = mean(Accuracy.FS.acc_ZN)*10;
    Err_GO_FS = mean(Accuracy.FS.acc_GO)*10;
    Err_DY_FS = mean(Accuracy.FS.acc_DY)*10;
    Err_DEV_FS = mean(Accuracy.FS.acc_DEV)*10;
    
    Err_AC1_FO = mean(Accuracy.FO.acc_AC1)*10;
    Err_AC2_FO = mean(Accuracy.FO.acc_AC2)*10;
    Err_ZN_FO = mean(Accuracy.FO.acc_ZN)*10;
    Err_GO_FO = mean(Accuracy.FO.acc_GO)*10;
    Err_DY_FO = mean(Accuracy.FO.acc_DY)*10;
    Err_DEV_FO = mean(Accuracy.FO.acc_DEV)*10;
    
    % 3.2 Detection of best accurate model for the overall session trials
    [~, best_FS_i] = min([abs(Err_AC1_FS), abs(Err_AC2_FS), abs(Err_AC3_FS), abs(Err_AC4_FS), abs(Err_ZN_FS), abs(Err_GO_FS), abs(Err_DY_FS)]);
    [~, best_FO_i] = min([abs(Err_AC1_FO), abs(Err_AC2_FO), abs(Err_ZN_FO), abs(Err_GO_FS), abs(Err_DY_FS)]);
    
    % 3.3 Join accuracies on array
    pre_FS = [(Err_AC1_FS), (Err_AC2_FS), (Err_AC3_FS), (Err_AC4_FS), (Err_ZN_FS), (Err_GO_FS), (Err_DY_FS)];
    pre_FO = [(Err_AC1_FO),(Err_AC2_FO),(Err_ZN_FO), (Err_GO_FS), (Err_DY_FS)];
    
    % 3.4 AS model is the one chosed at 3.2
    Err_AS_FS = pre_FS(best_FS_i);
    Err_AS_FO = pre_FO(best_FO_i);
    
    methods_FS = ["AC1", "AC2", "AC3", "AC4", "Zeni", "Ghoussayni", "Desailly"];
    methods_FO = ["AC5", "AC6", "Zeni", "Ghoussayni", "Desailly"];
    
    best_method_FS = methods_FS(best_FS_i);
    best_method_FO = methods_FO(best_FO_i);
    
    % numnber of training events on the overall session trials
    n_left_train_events = length(LEvFoot)-1;
    n_right_train_events = length(REvFoot)-1;
    total_train_events = n_left_train_events+ n_right_train_events;
    
    % Add session accuracies info into general table (for all sessions)
    tab_temp = table(string(list_sessions{s}), Err_AC1_FS, Err_AC2_FS, Err_AC3_FS, Err_AC4_FS, Err_ZN_FS, Err_GO_FS, Err_DY_FS, Err_AS_FS, Err_DEV_FS,best_method_FS, Err_AC1_FO, Err_AC2_FO,  Err_ZN_FO, Err_GO_FO, Err_DY_FO, Err_AS_FO, Err_DEV_FO, best_method_FO, n_left_train_events, n_right_train_events, total_train_events);
    Accuracy_tab = [Accuracy_tab; tab_temp];
    close all
    
end
toc
%% 4. Results visualization
writetable(Accuracy_tab, 'CPAccuracy10.xlsx');
Accuracy_CP = Accuracy_tab;
save('Accuracy_ITW');
T.Properties.VariableNames = {'Patient Session ID', 'Accuracy FS AC1', 'Accuracy FS AC2','Accuracy FS AC3','Accuracy FS AC4','Accuracy FS Zeni','Accuracy FS Ghoussayni','Accuracy FS Desailly','Accuracy FS AutoSelection', 'Best method FS', 'Accuracy FO AC5', 'Accuracy FO AC6', 'Accuracy FO Zeni', 'Accuracy FO Goussayni', 'Accuracy FO Desailly','Accuracy FO AutoSelected', 'Best method FO', 'Number left train events', 'Number right train events', 'Total train events'};

% 4.1 violin plot FS
fs_color = {[0 0.447 0.4],[0.1 0.5 0.55],[0.2 0.6 0.7],[0.3 0.75 0.85],[0.5 0.18 0.55],[0.85 0.33 0.1],[0.635 0.08 0.18],[0.47 0.67 0.19],[0.93 0.69 0.13]};
figure(1)
title('Accuracy foot strike (ms)')
vs = violinplot(Accuracy_tab(:,2:10), 'ShowNotches')
for j=1:length(fs_color)
    vs(j).ViolinColor = fs_color{j};
end
ylim([-300,300])
hold on
grid on
yline(0)
xticklabels({'AC1', 'AC2', 'AC3','AC4', 'ZEN', 'GHO', 'DES','AS', 'DEV'});
saveas(gcf, 'All Accuracy FS.pdf')
saveas(gcf, 'All Accuracy_FS.png')

% 4.2
fo_color = {[0 0.447 0.4],[0.1 0.5 0.55],[0.5 0.18 0.55],[0.85 0.33 0.1],[0.635 0.08 0.18],[0.47 0.67 0.19],[0.93 0.69 0.13]};
figure(2)
title('Accuracy foot off (ms)')
vs_fo = violinplot(Accuracy_tab(:,12:18));
for j=1:length(fo_color)
    vs_fo(j).ViolinColor = fo_color{j};
end
hold on
grid on
ylim([-300 300])
yline(0)
xticklabels({'AC5', 'AC6','ZEN', 'GHO','DES', 'AS', 'DEV'})
saveas(gcf, 'All Accuracy FO.pdf')
saveas(gcf, 'All Accuracy FO.png')

% 4.3 pie chart FS (precentage of models chosed for AS)
rep_fsm = table2array(Accuracy_tab(:,11))
fs_count = [sum(count(rep_fsm, "AC1")),sum(count(rep_fsm, "AC2")),sum(count(rep_fsm, "AC3")),sum(count(rep_fsm, "AC4")),sum(count(rep_fsm, "Zeni")),sum(count(rep_fsm, "Ghoussayni")), sum(count(rep_fsm, "Desailly"))]
figure(3)
title('Frequency of used methods FS')
scount = sum(fs_count);
p_fs = pie(fs_count, {strcat("AC1-",string(round((fs_count(1)/scount)*100,1)),"%"),strcat("AC2-",string(round((fs_count(2)/scount)*100,1)),"%"),strcat("AC3-",string(round((fs_count(3)/scount)*100,1)),"%"),strcat("AC4-",string(round((fs_count(4)/scount)*100,1)),"%"), strcat("Zeni-",string(round((fs_count(5)/scount)*100,1)),"%"), strcat("Ghoussayni-",string(round((fs_count(6)/scount)*100,1)),"%"), strcat("Desailly-",string(round((fs_count(7)/scount)*100,1)),"%")})
saveas(gcf, 'CP Percentage models FS.pdf')
saveas(gcf, 'CP Percentage models FS.png')

% 4.4 pie chart FO (percentage of models chosed for AS)
figure(4)
title('Frequency of used methods FO')
rep_fom = table2array(Accuracy_tab(:,19))
fo_count = [sum(count(rep_fom, "AC5")),sum(count(rep_fom, "AC6")),sum(count(rep_fom, "Zeni")),sum(count(rep_fom, "Ghoussayni")),sum(count(rep_fom, "Desailly"))];
p_fo = pie(fo_count, {strcat("AC5-",string(round((fo_count(1)/scount)*100,1)),"%"),strcat("AC6-",string(round((fo_count(2)/scount)*100,1)),"%"), strcat("Zeni-",string(round((fo_count(3)/scount)*100,1)),"%"), strcat("Ghoussayni-",string(round((fo_count(4)/scount)*100,1)),"%"), strcat("Desailly-",string(round((fo_count(5)/scount)*100,1)),"%")})
saveas(gcf, 'CP Percentage models FO.pdf')
saveas(gcf, 'CP Percentage models FO.png')

% 4.5 correlation between accuracy and number of training events
N_train = table2array((Accuracy_tab(:,22)))+1
figure(5)
title('Correlation accuracy vs number of training events')
scatter(table2array(Accuracy_tab(:,12)),N_train);
xlabel('Accuracy AC Foot Off')
ylabel('Number of training events')
saveas(gcf, 'CP Correl AC FO.pdf')
[R_FS, p] = corrcoef(table2array(Accuracy_tab(:,12)),N_train)

% 4.6 Build table with mean accuracy, std,confidence interval for all group
model = {'AC1', 'AC2', 'AC3','AC4', 'Zeni', 'Ghouss','Desail', 'AS', 'DEV'};
mean_FS = [mean(Accuracy_tab.Err_AC1_FS, 'omitnan'),mean(Accuracy_tab.Err_AC2_FS, 'omitnan'), mean(Accuracy_tab.Err_AC3_FS, 'omitnan'), mean(Accuracy_tab.Err_AC4_FS, 'omitnan'), mean(Accuracy_tab.Err_ZN_FS, 'omitnan'), mean(Accuracy_tab.Err_GO_FS, 'omitnan'), mean(Accuracy_tab.Err_DY_FS, 'omitnan'),mean(Accuracy_tab.Err_AS_FS, 'omitnan'), mean(Accuracy_tab.Err_DEV_FS, 'omitnan')];
std_FS = [std(Accuracy_tab.Err_AC1_FS, 'omitnan'),std(Accuracy_tab.Err_AC2_FS, 'omitnan'), std(Accuracy_tab.Err_AC3_FS, 'omitnan'), std(Accuracy_tab.Err_AC4_FS, 'omitnan'), std(Accuracy_tab.Err_ZN_FS, 'omitnan'), std(Accuracy_tab.Err_GO_FS, 'omitnan'),std(Accuracy_tab.Err_DY_FS, 'omitnan'), std(Accuracy_tab.Err_AS_FS, 'omitnan'), std(Accuracy_tab.Err_DEV_FS, 'omitnan')];
CI_FS = [ConfidenceInterval(Accuracy_CP.Err_AC1_FS); ConfidenceInterval(Accuracy_CP.Err_AC2_FS);ConfidenceInterval(Accuracy_CP.Err_AC3_FS); ConfidenceInterval(Accuracy_CP.Err_AC4_FS); ConfidenceInterval(Accuracy_CP.Err_ZN_FS); ConfidenceInterval(Accuracy_CP.Err_GO_FS); ConfidenceInterval(Accuracy_CP.Err_DY_FS); ConfidenceInterval(Accuracy_CP.Err_AS_FS); ConfidenceInterval(Accuracy_CP.Err_DEV_FS)];
mean_FO = [mean(Accuracy_tab.Err_AC1_FO, 'omitnan'),mean(Accuracy_tab.Err_AC2_FO, 'omitnan'),0, 0, mean(Accuracy_tab.Err_ZN_FO, 'omitnan'), mean(Accuracy_tab.Err_GO_FO, 'omitnan'),mean(Accuracy_tab.Err_DY_FO, 'omitnan'), mean(Accuracy_tab.Err_AS_FO, 'omitnan'), mean(Accuracy_tab.Err_DEV_FO, 'omitnan')];
std_FO = [std(Accuracy_tab.Err_AC1_FO, 'omitnan'), std(Accuracy_tab.Err_AC2_FO, 'omitnan'),0,0, std(Accuracy_tab.Err_ZN_FO, 'omitnan'), std(Accuracy_tab.Err_GO_FO, 'omitnan'), std(Accuracy_tab.Err_DY_FO, 'omitnan'),std(Accuracy_tab.Err_AS_FO, 'omitnan'), std(Accuracy_tab.Err_DEV_FO, 'omitnan')];
CI_FO = [ConfidenceInterval(Accuracy_CP.Err_AC1_FO); ConfidenceInterval(Accuracy_CP.Err_AC2_FO);[0, 0]; [0,0]; ConfidenceInterval(Accuracy_CP.Err_ZN_FO); ConfidenceInterval(Accuracy_CP.Err_GO_FO); ConfidenceInterval(Accuracy_CP.Err_DY_FO); ConfidenceInterval(Accuracy_CP.Err_AS_FO); ConfidenceInterval(Accuracy_CP.Err_DEV_FO)];
T2 = table(model',mean_FS',std_FS',CI_FS(:,1),CI_FS(:,2),mean_FO',std_FO',CI_FO(:,1),CI_FO(:,2), 'VariableNames', {'Model', 'mean FS', 'std FS','CI_FS_min','CI_FS_max','mean FO', 'std FO', 'CI_FO_min', 'CI_FO_max'})
writetable(T2,'CPmethod_accuracy.xlsx');

diag = zeros(length(Anthropo),1);
for i =1:length(Anthropo)
    x = string(Anthropo(i).Diagnostic);
    diag(i) = x;
end

Names = Accuracy_tab.Var1;
Patient_ID = {};Session_ID = {};
for i = 1:length(Names)
    name = Names{i};
    Patient_ID{end+1} = num2str(name(1:5));
    Session_ID{end+1} = num2str(name(7:11));
end
Export_tab = table(Patient_ID', Session_ID')
writetable(Export_tab, 'Export_info.xlsx')


% 4.7 Correlation Accuracy vs Gait Score
GS = readtable('2021-06-14_Export_INFO_FILEMAKER.csv');
ACC = readtable('CPAccuracy10.xlsx');
ID_pat = GS.ID_Patient;
ID_vis = GS.ID_Visite;
Var1 = {};
for i = 1:length(ID_pat)
    Var1{end+1} = strcat(sprintf('%05d', ID_pat(i)), '_',sprintf('%05d', ID_vis(i)));   
end
GS.Var1 = Var1';

newtable = innerjoin(GS, ACC, 'LeftKeys', [23], 'RightKeys', [1])
newtable.GPS = (newtable.LGPS + newtable.RGPS) / 2;
newtable.GDI = (newtable.LGDI + newtable.RGDI) / 2;
diagnostic = newtable.Diagnostic;
for i = 1: length(diagnostic)
   if convertCharsToStrings(diagnostic{i}) == 'CP_Spastic_Bi_Quadriplegia'
       diagnostic{i} = 'CP_Spastic_Bi_Diplegia';
   end
   if convertCharsToStrings(diagnostic{i}) == 'CP_Ataxic'
       diagnostic{i} = 'CP_Spastic_Bi_Diplegia';
   end
end
newtable.Diagnostic_mod = diagnostic;
figure(9)
gscatter((newtable.GPS),abs(newtable.Err_AC1_FS),newtable.Diagnostic_mod)
hold on

linFit = fitlm((newtable.GPS),abs(newtable.Err_AC1_FS),'Intercept',false);
ylabel('Accuracy (ms)')
xlabel('GPS')

figure(10)
gscatter((newtable.GDI),abs(newtable.Err_ZN_FS),newtable.GMFCS)
hold on

linFit = fitlm((newtable.GPS),abs(newtable.Err_AC1_FS),'Intercept',false);
ylabel('Accuracy (ms)')
xlabel('GDI')


% 4.8 Percentage of accuracy under x ms
ACC = readtable('AllAccuracy.xlsx');

u5 = [(sum(abs(ACC.Err_AC1_FS) < 5)/272*100); (sum(abs(ACC.Err_AC2_FS < 5))/272*100); (sum(abs(ACC.Err_AC3_FS) < 5)/272*100); (sum(abs(ACC.Err_AC4_FS < 5))/272*100); (sum(abs(ACC.Err_ZN_FS < 5))/272*100); (sum(abs(ACC.Err_GO_FS) < 5)/272*100); (sum(abs(ACC.Err_DY_FS) < 5)/272*100); (sum(abs(ACC.Err_AS_FS) < 5)/272*100); (sum(abs(ACC.Err_DEV_FS) < 5)/272*100); (sum(abs(ACC.Err_AC1_FO < 5))/272*100); (sum(abs(ACC.Err_AC2_FO < 5))/272*100); (sum(abs(ACC.Err_ZN_FO < 5))/272*100); (sum(abs(ACC.Err_GO_FO < 5))/272*100); (sum(abs(ACC.Err_DY_FO < 5))/272*100); (sum(abs(ACC.Err_AS_FO < 5))/272*100); (sum(abs(ACC.Err_DEV_FO < 5))/272*100)];
u10 = [(sum(abs(ACC.Err_AC1_FS < 10))/272*100); (sum(abs(ACC.Err_AC2_FS < 10))/272*100); (sum(abs(ACC.Err_AC3_FS < 10))/272*100); (sum(abs(ACC.Err_AC4_FS < 10))/272*100); (sum(abs(ACC.Err_ZN_FS < 10))/272*100); (sum(abs(ACC.Err_GO_FS < 10))/272*100); (sum(abs(ACC.Err_DY_FS < 10))/272*100); (sum(abs(ACC.Err_AS_FS < 10))/272*100); (sum(abs(ACC.Err_DEV_FS < 10))/272*100); (sum(abs(ACC.Err_AC1_FO < 10))/272*100); (sum(abs(ACC.Err_AC2_FO < 10))/272*100); (sum(abs(ACC.Err_ZN_FO < 10))/272*100); (sum(abs(ACC.Err_GO_FO < 10))/272*100); (sum(abs(ACC.Err_DY_FO < 10))/272*100); (sum(abs(ACC.Err_AS_FO < 10))/272*100); (sum(abs(ACC.Err_DEV_FO < 10))/272*100)];
u20 = [(sum(abs(ACC.Err_AC1_FS < 20))/272*100); (sum(abs(ACC.Err_AC2_FS < 20))/272*100); (sum(abs(ACC.Err_AC3_FS < 20))/272*100); (sum(abs(ACC.Err_AC4_FS < 20))/272*100); (sum(abs(ACC.Err_ZN_FS < 20))/272*100); (sum(abs(ACC.Err_GO_FS < 20))/272*100); (sum(abs(ACC.Err_DY_FS < 20))/272*100); (sum(abs(ACC.Err_AS_FS < 20))/272*100); (sum(abs(ACC.Err_DEV_FS < 20))/272*100); (sum(abs(ACC.Err_AC1_FO < 20))/272*100); (sum(abs(ACC.Err_AC2_FO < 20))/272*100); (sum(abs(ACC.Err_ZN_FO < 20))/272*100); (sum(abs(ACC.Err_GO_FO <20))/272*100); (sum(abs(ACC.Err_DY_FO < 20))/272*100); (sum(abs(ACC.Err_AS_FO <20))/272*100); (sum(abs(ACC.Err_DEV_FO <20))/272*100)];

list_models = {'AC1';'AC2';'AC3';'AC4';'ZN_FS';'GO_FS';'DY_FS';'AS_FS';'DEV_FS';'AC5';'AC6';'ZN_FO';'GO_FO';'DY_FO';'AS_FO';'DEV_FO'};

P_tab = table(list_models,u5, u10, u20)
writetable(P_tab, 'Percentage_accuracy_ms.xlsx')
