function c = FindLocalPeakAfterMax(c,vector, nframes, mpd_percent, mph_percent, mph_threshold)
% Function to find local peak right after a max peak
% vector: vector containing the data 
% nframes: number of frames of the trial
% mpd_percent: minimum peak diference in percentage of number of frames (default 0.2)
% mph_percent: minimum peak height in percentage to obtain the max peaks (default 0.5)
% mph_threshold: minimum peak heigth (y) in value to obtain the desirable peak
%
vector(1) = 0;
[b,a] = butter(4,6/(100/2));
vector=filtfilt(b,a,vector);
mx = max(vector);
[pks, locs] = findpeaks(vector, 'MinPeakDistance', nframes*mpd_percent, 'MinPeakHeight', mx*mph_percent);
locs = [locs; nframes];
amp = max(vector)-min(vector);
for p = 1:length(locs)-1
   for fr = locs(p)+1:locs(p+1)-2 
       slopea = abs(vector(fr)-vector(fr-1));
       slopeb = abs(vector(fr)- vector(fr+1));
       %        if vector(fr)<min(vector)+amp*0.1 && vector(fr+1)>vector(fr)
       if vector(fr)<mph_threshold && vector(fr+1)>vector(fr)
           %        if vector(fr)<min(vector)+amp*0.1 && slopea<slopeb
           c(fr-1)=c(fr-1)+1;
           break
       end
   end
end


end