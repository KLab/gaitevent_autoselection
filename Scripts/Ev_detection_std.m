function ev_frames = Ev_detection_std(parameters, Ref_std)



nframes = size(parameters,2);
ev_frames = zeros(nframes,1)';
for p = 1:size(parameters,1)
   for n = 1:nframes
      if parameters(p,n) < Ref_std(p)/5
          ev_frames(n) = ev_frames(n)+1;
      end
   end
end



end